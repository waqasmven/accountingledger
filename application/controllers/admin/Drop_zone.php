<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Drop_zone extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        /* $this->load->helper(array('url','file')); */
    }

    public function add_gallery() {
        $fullName = get_session_user_name();
        if ($fullName == '') {
            redirect(base_url('admin'));
        } else {
            $album_title = 'New Album_'. rand();
            $this->db->insert('albums', array('album_title' => $album_title));            
            $album_id = $this->db->insert_id();
            $this->session->set_userdata('Album_id', $album_id);
            $data['heading'] = 'Add Gallery';
            $this->load->view('admin/add_gallery', $data);
        }
    }

    //Untuk proses upload foto
    function process_upload() {
        $album_id = $this->session->userdata('Album_id');        
        $config['upload_path'] = GALLERYIMAGESPATH;
        $config['allowed_types'] = 'gif|jpg|png|ico';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $token = $this->input->post('token_foto');
            $token = $this->input->post('token_foto');
            $pic_name = $this->upload->data('file_name');
            $pic_path = GALLERYIMAGESPATH.$pic_name;
            
            $this->db->insert('gallery', array('album_id' => $album_id, 'token' => $token, 'pic_title' => $pic_name, 'pic_path' => $pic_path));
        }
    }

    //Untuk menghapus foto
    function remove_foto() {

        //Ambil token foto
        $token = $this->input->post('token');

        $foto = $this->db->get_where('gallery', array('token' => $token));

        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $pic_name = $hasil->pic_title;
            $pic_path = base_url(GALLERYIMAGESPATH . $pic_name);
            if (file_exists($file = $pic_name)) {
                unlink($file);
            }
            $this->db->delete('gallery', array('token' => $token));
        }


        echo "{}";
    }

}
