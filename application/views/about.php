<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('_header') ?>
    </head>

    <body>
        <!-- banner -->
        <div class="banner1">
            <div class="container">
                <?php $this->load->view('_top_nav'); ?>
            </div>
        </div>
        <!-- banner -->
        <!-- bootstrap-pop-up -->
        <div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        Qazi Agri farms
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <section>
                        <div class="modal-body">
                            <img src="<?= base_url(SITETHEME) ?>images/4.jpg" alt=" " class="img-responsive" />
                            <p><strong>To develop the existing infrastructure of horticulture sector especially in the field of vegetable yield by enabling small farmer to get more out of less resources by using latest trends and technologies in ‘Green House Tunnel Farming’.

                                    The aim and objectives of ‘Qazi Agri Farms’ additionally hope to:-

                                    •Advise and assist the farmers and workers by providing free consultancy services for promotion of ‘Green House Tunnel Farming’

                                    •Provide ample chances in training cadre to jobless (educated & un educated lot) and poor lot of marginalized sectors of our society

                                    •Transfer of technologies to worker class being utilized in different projects.

                                    •To promote own vegetable products globally</strong></p>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- //bootstrap-pop-up -->
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <div class="w3layouts_breadcrumbs_left">
                    <ul>
                        <li><i class="fa fa-home" aria-hidden="true"></i><a href="<?= base_url() ?>">Home</a><span>/</span></li>
                        <li><i class="fa fa-info-circle" aria-hidden="true"></i>About</li>
                    </ul>
                </div>
                <div class="w3layouts_breadcrumbs_right">
                    <h2>About Us</h2>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!-- //breadcrumbs -->
        <!-- about -->
        <div class="welcome">
            <div class="container">
                <h3 class="agileits_w3layouts_head">Why <span>Choose</span> Us</h3>
                <div class="w3_agile_image">
                    <img src="<?= base_url(SITETHEME) ?>images/1.png" alt=" " class="img-responsive" />
                </div>

                <div class="w3ls_news_grids">
                    <div class="col-md-6 w3_agile_about_grid_left">
                        <p>
                        <font color="#a0d034"> <h1>Qazi Agri Farms</h1></font> <font color="White">  envisions the vitalization of dynamic and market driven horticulture sector which is resilient, sustainable and responsive to meet the challenges of globalization.<br>

                        To develop the existing horticulture infrastructure especially in the field of vegetable capitulation by enabling common smaller farmers to get more out of less resources by adopting and promoting low tunnel & walk-in tunnel vegetable culture in the realm of green house tunnel farming technology.<br>
                        To give a better life exposure to a common farmer by empowering and utilizing their potential to make them fortunate by giving them awareness about the use of own natural available resources:-<br>
                        <h4>
                            <font color="White">•Full Utilization of four climatic conditions. Naturally available to Pakistan.<br>
                            •Use of ‘Green House Tunnel Farming’ by the small farmer effectively.<br>
                            •Provision of free consultancy services in agriculture field as well as Green House Tunnel Farming technology.<br>
                            • To bring revolution in the social status of common farmer.<br>

                            •Make them proficient in the knowledge of horticulture sector by enabling to promote their own vegetable products globally.<br>

                            •Promotion of Cultivation on small piece of land being awarded to him as resulted of inherited division of land by their family. </font></h4><br>

                        Last but not the least, we are making endeavour to remove the poverty from the country by utilizing the scope of our vision to the best our abilities.</font></p>
                    </div>


                    <!-- //about -->
                    <!-- skills -->
                    <script src="<?= base_url(SITETHEME) ?>js/skill.bars.jquery.js"></script>
                    <script>
                        $(document).ready(function () {

                            $('.skillbar').skillBars({
                                from: 0,
                                speed: 4000,
                                interval: 100,
                                decimals: 0,
                            });

                        });
                    </script>
                    <!-- //skills -->
                    <!-- about-bottom -->
                    <div class="about-bottom">
                        <div class="container">
                            <h3><span>Agriculture</span> not only gives riches to a nation, but the
                                only riches she can call her own</h3>
                            <div class="agileits_w3layouts_learn_more agileits_learn_more hvr-radial-out">
                                <a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!-- //about-bottom -->
                    <!-- team -->
                    <div class="welcome">
                        <div class="container">
                            <h3 class="agileits_w3layouts_head">Meet Our <span>Amazing</span> Team</h3>
                            <div class="w3_agile_image">
                                <img src="<?= base_url(SITETHEME) ?>images/1.png" alt=" " class="img-responsive" />
                            </div>

                            <div class="w3ls_news_grids w3_agileits_team_grids">
                                <div class="col-md-3 w3_agileits_team_grid">
                                    <div class="w3layouts_news_grid">
                                        <img src="<?= base_url(SITETHEME) ?>images/naeem.jpg" alt=" " class="img-responsive" />
                                        <div class="w3layouts_news_grid_pos">
                                            <div class="wthree_text agileinfo_about_text">
                                                <ul class="agileits_social_list">
                                                    <li><a href="https://www.facebook.com/qazi.naeemullah" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <h4>Qazi Naeem Ullah</h4>
                                    <p><strong>CEO</strong></p>
                                </div>
                                <div class="col-md-3 w3_agileits_team_grid">
                                    <div class="w3layouts_news_grid">
                                        <img src="<?= base_url(SITETHEME) ?>images/faisal.jpg" alt=" " class="img-responsive" />
                                        <div class="w3layouts_news_grid_pos">
                                            <div class="wthree_text agileinfo_about_text">
                                                <ul class="agileits_social_list">
                                                    <li><a href="https://www.facebook.com/muhammadfaisalOO7" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <h4>Faisal Qazi</h4>
                                    <p><strong>General Manager</strong></p>
                                </div>
                                <div class="col-md-3 w3_agileits_team_grid">
                                    <div class="w3layouts_news_grid">
                                        <img src="<?= base_url(SITETHEME) ?>images/hafi.jpg" alt=" " class="img-responsive" />
                                        <div class="w3layouts_news_grid_pos">
                                            <div class="wthree_text agileinfo_about_text">
                                                <ul class="agileits_social_list">
                                                    <li><a href="https://www.facebook.com/AbdulHafi93" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <h4>Abdul Hafi</h4>
                                    <p><strong>Vegetable & Fruit Inspector</strong></p>
                                </div>
                                <div class="col-md-3 w3_agileits_team_grid">
                                    <div class="w3layouts_news_grid">
                                        <img src="<?= base_url(SITETHEME) ?>images/ibtisam.jpg" alt=" " class="img-responsive" />
                                        <div class="w3layouts_news_grid_pos">
                                            <div class="wthree_text agileinfo_about_text">
                                                <ul class="agileits_social_list">
                                                    <li><a href="https://www.facebook.com/muhammad.ibtisam.9461" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <h4>Muhammad Ibtisam</h4>
                                    <p><strong>IT Manager </strong></p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <!-- //team -->
        <!-- footer -->
        <?php $this->load->view('_footer') ?>
        <!-- //footer -->
        <!-- menu -->
        <script>
            $(function () {

                initDropDowns($("div.shy-menu"));

            });

            function initDropDowns(allMenus) {

                allMenus.children(".shy-menu-hamburger").on("click", function () {

                    var thisTrigger = jQuery(this),
                            thisMenu = thisTrigger.parent(),
                            thisPanel = thisTrigger.next();

                    if (thisMenu.hasClass("is-open")) {

                        thisMenu.removeClass("is-open");

                    } else {

                        allMenus.removeClass("is-open");
                        thisMenu.addClass("is-open");
                        thisPanel.on("click", function (e) {
                            e.stopPropagation();
                        });
                    }

                    return false;
                });
            }
        </script>
        <!-- //menu -->
        <!-- start-smoth-scrolling -->
        <script type="text/javascript" src="<?= base_url(SITETHEME) ?>js/move-top.js"></script>
        <script type="text/javascript" src="<?= base_url(SITETHEME) ?>js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
        <!-- start-smoth-scrolling -->
        <!-- for bootstrap working -->
        <script src="<?= base_url(SITETHEME) ?>js/bootstrap.js"></script>
        <!-- //for bootstrap working -->
        <!-- here stars scrolling icon -->
        <script type="text/javascript">
            $(document).ready(function () {
                /*
                 var defaults = {
                 containerID: 'toTop', // fading element id
                 containerHoverID: 'toTopHover', // fading element hover id
                 scrollSpeed: 1200,
                 easingType: 'linear'
                 };
                 */

                $().UItoTop({easingType: 'easeOutQuart'});

            });
        </script>
        <!-- //here ends scrolling icon -->
        <script>
            $(document).ready(function () {
                $("#about").addClass(" active");
            });
        </script>
    </body>
</html>