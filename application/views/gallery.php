<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('_header'); ?>
    </head>

    <body>
        <!-- banner -->
        <div class="banner1">
            <div class="container">
                <?php $this->load->view('_top_nav'); ?>
            </div>
        </div>
        <!-- banner -->
        <!-- bootstrap-pop-up -->
        <div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        Qazi Agri Farms
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <section>
                        <div class="modal-body">
                            <img src="<?= base_url(SITETHEME) ?>images/4.jpg" alt=" " class="img-responsive" />
                            <p><strong>To develop the existing infrastructure of horticulture sector especially in the field of vegetable yield by enabling small farmer to get more out of less resources by using latest trends and technologies in ‘Green House Tunnel Farming’.

                                    The aim and objectives of ‘Qazi Agri Farms’ additionally hope to:-

                                    •Advise and assist the farmers and workers by providing free consultancy services for promotion of ‘Green House Tunnel Farming’

                                    •Provide ample chances in training cadre to jobless (educated & un educated lot) and poor lot of marginalized sectors of our society

                                    •Transfer of technologies to worker class being utilized in different projects.

                                    •To promote own vegetable products globally</strong></p>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- //bootstrap-pop-up -->
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <div class="w3layouts_breadcrumbs_left">
                    <ul>
                        <li><i class="fa fa-home" aria-hidden="true"></i><a href="<?= base_url() ?>">Home</a><span>/</span></li>
                        <li><i class="fa fa-picture-o" aria-hidden="true"></i>Gallery</li>
                    </ul>
                </div>
                <div class="w3layouts_breadcrumbs_right">
                    <h2>Gallery</h2>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!-- //breadcrumbs -->
        <!-- gallery -->
        <div class="welcome">
            <div class="container">
                <h3 class="agileits_w3layouts_head">Our <span>Gallery</span></h3>
                <div class="w3_agile_image">
                    <img src="<?= base_url(SITETHEME) ?>images/1.png" alt=" " class="img-responsive" />
                </div>
                <div class="w3layouts_gallery_grids">
                    <?php foreach ($gallery_data as $gallery_row) { ?>
                        <div class="col-md-4 w3layouts_gallery_grid">
                            <a href="<?= base_url() . $gallery_row->pic_path ?>" class="lsb-preview" data-lsb-group="header">
                                <div class="w3layouts_news_grid">
                                    <img src="<?= base_url() . $gallery_row->pic_path ?>" alt="" class="img-responsive">
                                    <div class="w3layouts_news_grid_pos">
                                        <div class="wthree_text"><h3><?= $gallery_row->pic_title ?></h3></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="clearfix"> </div>
                    <?php echo $links; ?>
                </div>
            </div>
        </div>
        <!-- //gallery -->
        <script src="<?= base_url(SITETHEME) ?>js/lsb.min.js"></script>
        <script>
            $(window).load(function () {
                $.fn.lightspeedBox();
            });
        </script>
        <!-- footer -->
        <?php $this->load->view('_footer') ?>
        <!-- menu -->
        <script>
            $(function () {

                initDropDowns($("div.shy-menu"));

            });

            function initDropDowns(allMenus) {

                allMenus.children(".shy-menu-hamburger").on("click", function () {

                    var thisTrigger = jQuery(this),
                            thisMenu = thisTrigger.parent(),
                            thisPanel = thisTrigger.next();

                    if (thisMenu.hasClass("is-open")) {

                        thisMenu.removeClass("is-open");

                    } else {

                        allMenus.removeClass("is-open");
                        thisMenu.addClass("is-open");
                        thisPanel.on("click", function (e) {
                            e.stopPropagation();
                        });
                    }

                    return false;
                });
            }
        </script>
        <!-- //menu -->
        <!-- start-smoth-scrolling -->
        <script type="text/javascript" src="<?= base_url(SITETHEME) ?>js/move-top.js"></script>
        <script type="text/javascript" src="<?= base_url(SITETHEME) ?>js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
        <!-- start-smoth-scrolling -->
        <!-- for bootstrap working -->
        <script src="<?= base_url(SITETHEME) ?>js/bootstrap.js"></script>
        <!-- //for bootstrap working -->
        <!-- here stars scrolling icon -->
        <script type="text/javascript">
            $(document).ready(function () {
                /*
                 var defaults = {
                 containerID: 'toTop', // fading element id
                 containerHoverID: 'toTopHover', // fading element hover id
                 scrollSpeed: 1200,
                 easingType: 'linear'
                 };
                 */

                $().UItoTop({easingType: 'easeOutQuart'});

            });
        </script>
        <!-- //here ends scrolling icon -->
        <script>
            $(document).ready(function () {
                $("#gallery").addClass(" active");
            });
        </script>
    </body>
</html>