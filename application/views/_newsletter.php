<!-- newsletter -->
	<div class="newsletter">
		<div class="container">
			<h3 class="agileits_w3layouts_head agileinfo_head"><span>Subscribe</span> to our newsletter</h3>
			<div class="w3_agile_image">
				<img src="<?= base_url(SITETHEME) ?>images/12.png" alt=" " class="img-responsive">
			</div>
			<p class="agile_para agileits_para">Morbi viverra lacus commodo felis semper, eu iaculis lectus nulla at sapien blandit sollicitudin.</p>
			<div class="w3ls_news_grids w3ls_newsletter_grids">
				<form action="#" method="post">
					<input name="Your Name" placeholder="Your Name" type="text" required="">
					<input name="Your Email" placeholder="Your Email" type="email" required="">
					<input type="submit" value="Subscribe">
				</form>
			</div>
		</div>
	</div>
<!-- //newsletter -->