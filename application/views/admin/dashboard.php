<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('admin/_header'); ?>

    <body class="hold-transition sidebar-mini">

        <div class="wrapper">

            <!-- Main Sidebar Container -->
            <?php $this->load->view('admin/_side_bar'); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view('admin/_bread_crumbs'); ?>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header border-0">
                                        <div class="d-flex justify-content-between">
                                            <h3 class="card-title">Index Page Content Update</h3>
                                            <!--<a href="javascript:void(0);">View Report</a>-->
                                        </div>
                                    </div>
                                    <form enctype="multipart/form-data"  name="main_page_form" id="main_page_form" method="post">
                                        <div class="card">

                                            <div class="card-body table-responsive p-0">
                                                <table class="table table-striped table-valign-middle">
                                                    <thead>
                                                        <tr>
                                                            <th>Heading </th>
                                                            <th>Detail</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" type="text" name="h1" id="h1" value="<?= $main_page_data['h1'] ?>"/>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">

                                                                    <textarea class="form-control" rows="3" name="h1_detail" id="h1_detail" ><?= $main_page_data['h1_detail'] ?></textarea>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" type="text" name="h2" id="h2" value="<?= $main_page_data['h2'] ?>"/>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <textarea class="form-control" name="h2_detail" id="h2_detail" rows="3" ><?= $main_page_data['h2_detail'] ?></textarea>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" type="text" name="h3" id="h3" value="<?= $main_page_data['h3'] ?>"/>
                                                            </td>
                                                            <td>
                                                                <div class="form-group">

                                                                    <textarea class="form-control" name="h3_detail" id="h3_detail" rows="3"><?= $main_page_data['h3_detail'] ?></textarea>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" type="text" name="h4" id="h4" value="<?= $main_page_data['h4'] ?>"/>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="row">
                                                <div class="col-8">
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-4">
                                                    <button type="submit" class="btn btn-primary btn-block">Update</button>
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <div id="msg" class="h-75">

                                            </div>
                                        </div>
                                        <!-- /.card -->
                                    </form>
                                    <!-- /End Main Page form -->
                                </div>
                                <!-- /.col-md-6 -->
                                <!-- /.col-md-6 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content -->
                </div>
            </div>
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('admin/_footer') ?>

        <script>
            $(document).ready(function () {
                $("#main_page_form").validate({
                    rules: {
                        h1: {required: true},
                        h2: {required: true},
                        h3: {required: true},
                        h4: {required: true},
                        h1_detail: {required: true},
                        h2_detail: {required: true},
                        h3_detail: {required: true},

                        pass: {required: true}
                    },
                    highlight: function (element, errorClass) {
                        $(element).css({borderColor: '#FF0000'});
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).css({borderColor: '#CCCCCC'});
                    },
                    //messages: {email: "Please enter a valid email address"},
                    submitHandler: function (form) {
                        $.ajax({
                            type: "POST",
                            url: "<?= base_url('dashboard/update/main-page-text') ?>",
                            data: $('#main_page_form').serialize(),
                            success: function (data) {
                                var response = jQuery.parseJSON(data);
                                if (response.status === 'Success') {
                                    document.getElementById("msg").innerHTML = '<h3>'+response.msg+'</h3>';
                                    $('#msg').css({color: 'Green'});
                                    $('#msg').css({borderColor: 'Green'});
                                    window.setTimeout(function () {
                                        location.href = response.redirect;
                                    }, 2000);
                                } else if (response.status == 'error') {
                                    document.getElementById("msg").innerHTML = '<h3>'+response.msg+'</h3>';
                                    $('#msg').css({color: 'Red'});
                                    $('#msg').css({borderColor: 'Red'});
                                }
                            }
                        });
                        return false;
                    }
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $("#Dashboard").addClass(" active");
            });
        </script>
    </body>
</html>
