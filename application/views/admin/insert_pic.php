<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('admin/_header'); ?>

    <body class="hold-transition sidebar-mini">

        <div class="wrapper">
            <!-- Main Sidebar Container -->
            <?php $this->load->view('admin/_side_bar'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view('admin/_bread_crumbs'); ?>
                <!-- /.content-header -->
                <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header border-0">
                                        <div class="d-flex justify-content-between">
                                            <h3 class="card-title">Index Page Content Update</h3>
                                            <!--<a href="javascript:void(0);">View Report</a>-->
                                        </div>
                                    </div>
                                    <form enctype="multipart/form-data"  action="" name="insert_slide_pic_form" id="insert_slide_pic_form" method="post">
                                        <div class="card">
                                            <div class="card-body table-responsive p-0">
                                                <table class="table table-striped table-valign-middle">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" type="text" name="pic_title" id="pic_title" placeholder="Enter Picture Title"/>
                                                            </td>
                                                            <td>
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" id="userfile" name="userfile">
                                                                    <label class="custom-file-label" for="file">Choose file</label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="row">
                                                <div class="col-8">
                                                </div>
                                                <div class="col-4">
                                                    <button type="submit" class="btn btn-success btn-block">Insert</button>
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <div id="msg" class="h-75""></div>
                                        </div>
                                        <!-- /.card -->
                                    </form>
                                    <!-- /End Main Page form -->
                                </div>
                                <!-- /.col-md-6 -->
                                <!-- /.col-md-6 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->
            </div>
        </div>
        <?php $this->load->view('admin/_footer') ?>
        <script>
            $(document).ready(function () {
                $("#insert_slide_pic_form").submit(function (e) {
                    e.preventDefault();
                }).validate({
                    rules: {
                        pic_title: {required: true},
                        userfile: {required: true}

                    },
                    highlight: function (element, errorClass) {
                        $(element).css({borderColor: '#FF0000'});
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).css({borderColor: '#CCCCCC'});
                    },
                    //errorPlacement: function (error, element) {$.validator.messages.required = '';},
                    messages: {conf_new_pass: "New password and Confirm password do not match!"},
                    //invalidHandler: function(form, validator) {},
                    submitHandler: function (form) {
                        $('#loader').show();
                        var formData = new FormData($('form')[0]);
                        $.ajax({
                            type: "POST",
                            url: "<?= base_url('admin/Admin_con/add_new_slide_pic') ?>",
                            enctype: 'multipart/form-data',
                            data: formData,
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function (data) {
                                $('#loader').fadeOut(2000);
                                var response = jQuery.parseJSON(data);
                                if (response.status === 'Success') {
                                    Swal.fire("Picture Successfully Added",'','success');
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                } else {
                                    Swal.fire("Something went wrong! Try Again",'','error');
                                }
                            }
                        });
                        // return false;
                    }
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $("#slideshow").addClass(" active");
                $("#insert_ss").addClass(" active");
            });
        </script>

    </body>
</html>
