<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('admin/_header'); ?>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <!-- Main Sidebar Container -->
            <?php $this->load->view('admin/_side_bar'); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view('admin/_bread_crumbs'); ?>
                <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h4 class="card-title">Slideshow Pictures</h4>
                                    </div>
                                    <div class="card-body">
                                        <?php if (empty($slide_show_data)) { ?>
                                            <div class="row"><?= NODATA ?></div>
                                        <?php } else { ?>
                                            <div class="row">
                                                <?php
                                                foreach ($slide_show_data as $slide_row) {
                                                    //$print_r($slide_row); die;
                                                    $pic_id = $slide_row['id'];
                                                    $pic_source = base_url(UPLOADIMAGESPATH) . $slide_row['pic_path'];
                                                    $pic_title = $slide_row['pic_title'];
                                                    ?>
                                                    <div class="col-sm-3">
                                                        <a href="<?= $pic_source ?>" title="<?= $pic_title ?>" data-toggle="lightbox" data-title="sample 1 - white" data-gallery="gallery">
                                                            <img height="250" width="250" src="<?= $pic_source ?>" class="img-fluid mb-2 img-rounded img-circle" alt="<?= $pic_title ?>"/>
                                                        </a>
                                                        <div class="row">
                                                            <a href="<?= base_url('dashboard/view/slide-show-pic/' . $pic_id) ?>" class="btn btn-sm btn-danger" ><i class="far fa-edit btn"></i></a> &nbsp; 	&nbsp;
                                                            <a href="javascript:void(0)" class="btn btn-sm btn-primary" onclick="delete_photo('<?= $pic_id ?>', '<?= strtoupper($pic_title) ?>')"><i  class="fas fa-trash-alt"></i></a>
                                                        </div>
                                                    </div>
                                                <?php }
                                            } ?>

                                        </div>
                                    </div>
                                    <div id="msg"></div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>
            </div>
        </div>

        <!-- /.content-wrapper -->
<?php $this->load->view('admin/_footer') ?>
        <script>
            function delete_photo(photo_id, photo_name) {
                Swal.fire({
                    title: "Are you sure you want to delete \"" + photo_name + "\"? You can't revert this action!",
                    showDenyButton: true,
                    showCancelButton: true,
                    confirmButtonText: `Confirm`,
                    denyButtonText: `Cancel`,
                    customClass: {
                        confirmButton: 'btn btn-danger',
                    },
                }).then((result) => {
                    if (result.value === true) {
                        $.ajax({
                            type: "POST",
                            data: {id: photo_id},
                            url: '<?= base_url('admin/Admin_con/delete_slide_show_pic/') ?>',
                            success: function (result)
                            {
                                Swal.fire(photo_name + " Successfully Deleted", '', 'success');
                                setTimeout(function () {
                                    location.reload();
                                }, 1000);
                            }
                        });
                    } else {
                        Swal.fire('Changes are not saved', '', 'info');
                        return false;
                    }

                })

            }
        </script>

        <script>
            $(document).ready(function () {
                $("#slideshow").addClass(" active");
                $("#view_ss").addClass(" active");
            });
        </script>
    </body>
</html>
