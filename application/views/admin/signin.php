<html lang="en">
    <?php $this->load->view('admin/_header'); ?>
    <body class="hold-transition login-page">
        <div class="login-box">
            <!-- /.login-logo -->
            <div class="card card-outline card-primary">
                <div class="card-header text-center">
                    <a href="<?php echo base_url(); ?>" class="h1"><b>QaziAgri</b>Farms</a>
                </div>
                <div class="card-body">
                    <p class="login-box-msg">Sign in to start your session</p>

                    <form id="login_form" name="login_form" method="POST" enctype="multipart/form-data">
                        <div class="input-group mb-3">
                            <input type="text" name="username" id="username" class="form-control" placeholder="Email">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" name="pass" id="pass" class="form-control" placeholder="Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">

                            </div>
                            <!-- /.col -->
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                            </div>
                            <div id="msg" class="card">

                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                    <p class="mb-1">
                        <!-- <a href="forgot-password.html">I forgot my password</a> -->
                    </p>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery -->
        <script src="<?php echo base_url(ADMINTHEME); ?>plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="<?php echo base_url(ADMINTHEME); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(ADMINTHEME); ?>dist/js/adminlte.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
        <script>
            $(document).ready(function () {
                $("#login_form").validate({
                    rules: {
                        username: {required: true},
                        pass: {required: true}
                    },
                    highlight: function (element, errorClass) {
                        $(element).css({borderColor: '#FF0000'});
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).css({borderColor: '#CCCCCC'});
                    },
                    //messages: {email: "Please enter a valid email address"},
                    submitHandler: function (form) {
                        $.ajax({
                            type: "POST",
                            url: "<?= base_url('admin') ?>/Admin_con/login",
                            data: $('#login_form').serialize(),
                            success: function (data) {
                                var response = jQuery.parseJSON(data);
                                if (response.status === 'Success') {
                                    Swal.fire("<?=LOGIN_SUCC_MSG?>",'','success');
                                    window.setTimeout(function () {
                                        location.href = response.redirect;
                                    }, 2000);
                                } else if (response.status == 'error') {
                                   Swal.fire("<?=LOGIN_FAIL_MSG?>",'',"error");

                                }
                            }
                        });
                        return false;
                    }
                });
            });
        </script>
    </body>
</html>
