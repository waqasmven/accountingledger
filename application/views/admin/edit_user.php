<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('admin/_header') ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <!-- Main Sidebar Container -->
            <?php $this->load->view('admin/_side_bar'); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view('admin/_bread_crumbs'); ?>

                <!-- Main content -->
                <section class="content">

                    <!-- Default box -->
                    <div class="card card-solid">
                        <div class="card-body pb-0">
                            <div class="col-md-6">
                                <!-- general form elements -->
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title"><?= $heading ?></h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form  enctype="multipart/form-data"  action="" name="edit_user_form" id="edit_user_form" method="post">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="full_name">Full Name</label>
                                                <input type="text" class="form-control" id="full_name" value="<?= $user_data->Full_Name ?>" name="full_name" placeholder="Enter Full Name">
                                                <input type="hidden" class="form-control" id="id" name="id" value="<?= $user_data->u_id ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email </label>
                                                <input type="email" class="form-control" id="email" name="email" value="<?= $user_data->email ?>" placeholder="Enter email">
                                            </div>
                                            <div class="form-group">
                                                <label for="adress">Address </label>
                                                <input type="text" class="form-control" id="address" name="address" value="<?= $user_data->address ?>" placeholder="Enter Your Address">
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Telephone </label>
                                                <input type="tel" max="11" class="form-control" id="phone" value="0<?= $user_data->phone ?>" name="phone" placeholder="Enter Phone No">
                                            </div>
                                            <div class="form-group">
                                                <label>Select</label>
                                                <select class="form-control" id="user_type" name="user_type">
                                                    <option value="0">Select Designation</option>
                                                    <?php foreach ($user_type as $type_row) { ?>
                                                        <option value="<?= $type_row->id ?>"> <?= $type_row->type ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                            </div>
                                            <div class="form-group">
                                                <label for="userfile">Profile Picture</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="userfile" name="userfile">
                                                        <label class="custom-file-label" for="userfile">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" class="form-control" id="p_pic" name="p_pic" value="<?= $user_data->p_pic ?>">
                                        <!-- /.card-body -->
                                        <div id="msg" class="h-75""></div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-success float-right">Update</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <?php $this->load->view('admin/_footer') ?>
            <script>
                $(document).ready(function () {
                    $("#edit_user_form").submit(function (e) {
                        e.preventDefault();
                    }).validate({
                        rules: {
                            password: {required: true, minlength: 6},
                            full_name: {required: true},
                            email: {required: true, email: true},
                            phone: {required: true, minlength: 11, maxlength: 11, digits: true},
                            user_type: {required: true, min: 1},
                            address: {required: true}

                        },
                        highlight: function (element, errorClass) {
                            $(element).css({borderColor: '#FF0000'});
                        },
                        unhighlight: function (element, errorClass, validClass) {
                            $(element).css({borderColor: '#CCCCCC'});
                        },
                        //errorPlacement: function (error, element) {$.validator.messages.required = '';},
                        messages: {conf_new_pass: "New password and Confirm password do not match!"},
                        //invalidHandler: function(form, validator) {},
                        submitHandler: function (form) {
                            $('#loader').show();
                            var formData = new FormData($('form')[0]);
                            $.ajax({
                                type: "POST",
                                url: "<?= base_url('admin/Admin_con/edit_user_data') ?>",
                                enctype: 'multipart/form-data',
                                data: formData,
                                contentType: false,
                                cache: false,
                                processData: false,
                                success: function (data) {
                                    $('#loader').fadeOut(2000);
                                    var response = jQuery.parseJSON(data);
                                    if (response.status === 'Success') {
                                        document.getElementById("msg").innerHTML = '<h3>' + response.msg + '</h3>';
                                        $('#msg').css({color: 'Green'});
                                        $('#msg').css({borderColor: 'Green'});
                                        window.setTimeout(function () {
                                            location.reload();
                                        }, 2000);
                                    } else {
                                        document.getElementById("msg").innerHTML = '<h3>' + response.msg + '</h3>';
                                        $('#msg').css({color: 'Red'});
                                        $('#msg').css({borderColor: 'Red'});
                                    }
                                }
                            });
                            // return false;
                        }
                    });
                });
            </script>
            <script>
                $(document).ready(function () {
                    $("#user_manage").addClass(" active");
                    $("#add_user").addClass(" active");
                });
            </script>
    </body>
</html>
