<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('admin/_header'); ?>

    <body class="hold-transition sidebar-mini">

        <div class="wrapper">
            <!-- Main Sidebar Container -->
            <?php $this->load->view('admin/_side_bar'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view('admin/_bread_crumbs'); ?>
                <!-- /.content-header -->
                <!-- Main content -->
                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h4 class="card-title">GALLERY</h4>
                                    </div>
                                    <div class="card-body">
                                       <div>
                                           <?php if(!empty($albums_data)) { ?>
                                            <div class="btn-group w-100 mb-2">
                                                <a class="btn btn-info active" href="javascript:void(0)" data-filter="all"> All items </a>
                                                <?php foreach ($albums_data as $album_row) { ?>
                                                    <a class="btn btn-info" href="javascript:void(0)" data-filter="<?= $album_row['id'] ?>"> <?= $album_row['album_title'] ?> </a>
                                                    <a class="btn btn-info active" href="javascript:void(0)" onclick="delete_photo('<?= $album_row['id'] ?>', '<?= $album_row['album_title'] ?>')"><i class="far fa-trash-alt" ></i> </a>
                                                <?php } ?>
                                           </div> <?php }else { ?>
                                           <div><?=NODATA?></div>
                                           <?php } ?>
                                        </div> 
                                        <div>
                                            <div class="filter-container p-0 row">
                                                <?php foreach ($gallery_data as $gallery_row) { ?>
                                                    <div class="filtr-item col-sm-4" data-category="<?= $gallery_row['album_id'] ?>" data-sort="white sample">
                                                        <a href="<?= base_url($gallery_row['pic_path']) ?>" title="<?= $gallery_row['pic_title'] ?>" data-toggle="ligtbox" data-title="sample 1 - white">
                                                            <img height="150" width="150" src="<?= base_url($gallery_row['pic_path']) ?>" class="img-fluid mb-2" alt="white sample"/>
                                                        </a>
                                                        <a href="<?= base_url('dashboard/view/gallery-pic/'.$gallery_row['id']) ?>"><i class="far fa-edit btn"></i> </a></a>
                                                        <a onclick="delete_gallery_photo('<?= $gallery_row['id'] ?>', '<?= $gallery_row['pic_title'] ?>')"><i class="far fa-trash-alt btn" ></i> </a>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->
                <div id="msg"></div>
                <!-- /.content-wrapper -->
            </div>
        </div>
        <?php $this->load->view('admin/_footer') ?>
        
        <script>
            $(document).ready(function () {
                $("#gallery").addClass(" active");
                $("#view_gallery").addClass(" active");
            });
        </script>
        <!-- Ekko Lightbox -->
        <script src="<?= base_url() . ADMINTHEME ?>/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?= base_url() . ADMINTHEME ?>/dist/js/adminlte.min.js"></script>
        <!-- Filterizr-->
        <script src="<?= base_url() . ADMINTHEME ?>/plugins/filterizr/jquery.filterizr.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?= base_url() . ADMINTHEME ?>/dist/js/demo.js"></script>
        <!-- Page specific script -->
        <script>
            $(function () {
                $(document).on('click', '[data-toggle="lightbox"]', function (event) {
                    event.preventDefault();
                    $(this).ekkoLightbox({
                        alwaysShowClose: true
                    });
                });

                $('.filter-container').filterizr({gutterPixels: 3});
                $('.btn[data-filter]').on('click', function () {
                    $('.btn[data-filter]').removeClass('active');
                    $(this).addClass('active');
                });
            })
        </script>
        
        <script>
                function delete_photo(album_id, album_name) {
                   
                    Swal.fire({
                        title: "Are you want to delete Album " + album_name + "? You can't revert this action!",
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: `Confirm`,
                        denyButtonText: `Cancel`,
                    }).then((result) => {
                        if (result.value === true) {
                            $.ajax({
                                type: "POST",
                                data: {id: album_id},
                                url: '<?= base_url('admin/Admin_con/delete_album_by_id/') ?>',
                                success: function (result)
                                {
                                    Swal.fire("Album Successfully Deleted", '', 'success');
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                            });
                        } else {
                            Swal.fire('Changes are not saved', '', 'info')
                        }

                    })

                }
            </script>

        <script type="text/javascript">
            function delete_gallery_photo(photo_id, photo_name) {
                var answer = confirm("Are you want to delete  " + photo_name + "? You can't revert this action!");
                if (answer) {
                    /* $.get("<?= base_url('admin/Admin_con/delete_slide_show_pic/') ?>", function (data, status) {
                     alert("Data: " + data + "\nStatus: " + status);
                     });*/
                    $.ajax({
                        type: "POST",
                        url: '<?= base_url('admin/Admin_con/delete_gallery_photo_by_id/') ?>',
                        data: {
                            id: photo_id
                        },
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            }
        </script>
    </body>
</html>
