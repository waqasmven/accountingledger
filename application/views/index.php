<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('_header') ?>
    </head>
    <body>
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v10.0" nonce="C6ekvAUk">
        </script><!-- Facebook -->
        
        
        <!-- banner -->
        <div class="banner">
            <div class="container">
                <?php $this->load->view('_top_nav'); ?>
                <div class="w3_banner_info">
                    <div class="w3_banner_info_grid">
                        <h3 class="test">I'm Planting A Tree To Teach Me To Gather Strength From My Deepest Roots</h3>	<br>
                        <ul>
                            <li><a href="<?= base_url('Contact-us') ?>" class="w3l_contact"><font color="white"><strong>Contact Us</strong></font></a></li>
                            <li><a href="#" class="w3ls_more" data-toggle="modal" data-target="#myModal">Read More</a></li>
                        </ul>
                    </div>
                </div>
                <div class="thim-click-to-bottom">
                    <a href="#welcome_bottom" class="scroll">
                        <i class="fa  fa-chevron-down"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- banner -->
        <!-- bootstrap-pop-up -->
        <div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        Qazi Agri Farms
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <section>
                        <div class="modal-body">
                            <img src="<?= base_url(SITETHEME) ?>images/4.jpg" alt=" " class="img-responsive" />
                            <p><strong>To develop the existing infrastructure of horticulture sector especially in the field of vegetable yield by enabling small farmer to get more out of less resources by using latest trends and technologies in ‘Green House Tunnel Farming’.

                                    The aim and objectives of ‘Qazi Agri Farms’ additionally hope to:-

                                    •Advise and assist the farmers and workers by providing free consultancy services for promotion of ‘Green House Tunnel Farming’

                                    •Provide ample chances in training cadre to jobless (educated & un educated lot) and poor lot of marginalized sectors of our society

                                    •Transfer of technologies to worker class being utilized in different projects.

                                    •To promote own vegetable products globally</strong>
                            </p>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- //bootstrap-pop-up -->
        <!-- banner-bottom -->
        <div class="banner-bottom">
            <div class="col-md-4 agileits_banner_bottom_left">
                <div class="agileinfo_banner_bottom_pos">
                    <div class="w3_agileits_banner_bottom_pos_grid">
                        <div class="col-xs-4 wthree_banner_bottom_grid_left">
                            <div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
                                <i class="fa fa-pagelines" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-xs-8 wthree_banner_bottom_grid_right">
                            <h4><?= $main_page_data['h1'] ?></h4>
                            <p><?= $main_page_data['h1_detail'] ?></p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 agileits_banner_bottom_left1">
                <div class="agileinfo_banner_bottom_pos">
                    <div class="w3_agileits_banner_bottom_pos_grid">
                        <div class="col-xs-4 wthree_banner_bottom_grid_left">
                            <div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
                                <i class="fa fa-certificate" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-xs-8 wthree_banner_bottom_grid_right">
                            <h4><?= $main_page_data['h2'] ?></h4>
                            <p><?= $main_page_data['h2_detail'] ?></p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 agileits_banner_bottom_left2">
                <div class="agileinfo_banner_bottom_pos">
                    <div class="w3_agileits_banner_bottom_pos_grid">
                        <div class="col-xs-4 wthree_banner_bottom_grid_left">
                            <div class="agile_banner_bottom_grid_left_grid hvr-radial-out">
                                <i class="fa fa-yelp" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-xs-8 wthree_banner_bottom_grid_right">
                            <h4><?= $main_page_data['h3'] ?></h4>
                            <p><?= $main_page_data['h3_detail'] ?></p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <!-- //banner-bottom -->
        <!-- welcome -->
        <div class="welcome">
            <div class="container">
                <h3 class="agileits_w3layouts_head"> <span><?= $main_page_data['h4']?></span></h3>
                <div class="w3_agile_image">
                    <img src="<?= base_url(SITETHEME) ?>images/1.png" alt=" " class="img-responsive" />
                </div>

            </div>
            <div class="mis-stage w3_agileits_welcome_grids">
                <!-- The element to select and apply miSlider to - the class is optional -->
                <ol class="mis-slider">
                    <?php
                    foreach ($slide_data as $slide_row) {
                        $img_path = UPLOADIMAGESPATH . $slide_row['pic_path'];
                        ?>
                        <li class="mis-slide">
                            <figure>
                                <img height="360" width="360" src="<?= base_url($img_path) ?>" alt="<?php echo $slide_row['pic_title']; ?> is missing" class="img-responsive img-circle img-rounded" style="border-radius: 50%"/>
                                <figcaption><?php echo $slide_row['pic_title']; ?></figcaption>
                            </figure>
                        </li>
                    <?php } ?>
                </ol>
            </div>
        </div>
        <!-- //welcome -->
        <!-- welcome-bottom -->
        <div id="welcome_bottom" class="welcome-bottom">
            <div class="col-md-6 wthree_welcome_bottom_left">
                <h3>we work hard and make our country <span>greenery</span></h3>

                <div class="col-md-6 wthree_welcome_bottom_left_grid">
                    <div class="w3l_social_icon_gridl">
                        <img src="<?= base_url(SITETHEME) ?>images/8.png" alt=" " class="img-responsive" />
                    </div>
                    <div class="w3l_social_icon_gridr">
                        <h4 class="counter">23,536</h4>
                    </div>
                    <div class="clearfix"> </div>
                    <div class="w3l_social_icon_grid_pos">
                        <label>-</label>
                    </div>
                </div>
                <div class="col-md-6 wthree_welcome_bottom_left_grid">
                    <div class="w3l_social_icon_gridl">
                        <img src="<?= base_url(SITETHEME) ?>images/9.png" alt=" " class="img-responsive" />
                    </div>
                    <div class="w3l_social_icon_gridr">
                        <h4 class="counter">53,234</h4>
                    </div>
                    <div class="clearfix"> </div>
                    <div class="w3l_social_icon_grid_pos">
                        <label>-</label>
                    </div>
                </div>
                <div class="col-md-6 wthree_welcome_bottom_left_grid">
                    <div class="w3l_social_icon_gridl">
                        <img src="<?= base_url(SITETHEME) ?>images/10.png" alt=" " class="img-responsive" />
                    </div>
                    <div class="w3l_social_icon_gridr">
                        <h4 class="counter">43,568</h4>
                    </div>
                    <div class="clearfix"> </div>
                    <div class="w3l_social_icon_grid_pos">
                        <label>-</label>
                    </div>
                </div>
                <div class="col-md-6 wthree_welcome_bottom_left_grid">
                    <div class="w3l_social_icon_gridl">
                        <img src="<?= base_url(SITETHEME) ?>images/11.png" alt=" " class="img-responsive" />
                    </div>
                    <div class="w3l_social_icon_gridr">
                        <h4 class="counter">12,432</h4>
                    </div>
                    <div class="clearfix"> </div>
                    <div class="w3l_social_icon_grid_pos">
                        <label>-</label>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-6 wthree_welcome_bottom_right">
                <div class="agileinfo_grid">
                    <figure class="agileits_effect_moses">
                        <img src="<?= base_url(SITETHEME) ?>images/4.jpg" alt=" " class="img-responsive" />
                        <figcaption>
                            <h4>Plantation <span>For Future Growth</span></h4>

                        </figcaption>
                    </figure>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <!-- //welcome-bottom -->
        <!-- news -->
        <div class="welcome">
            <div class="container">
                <h3 class="agileits_w3layouts_head">Latest <span>News</span> from plantation</h3>
                <div class="w3_agile_image">
                    <img src="<?= base_url(SITETHEME) ?>images/1.png" alt=" " class="img-responsive">
                </div>

                <div class="w3ls_news_grids">
                    <div class="col-md-4 w3ls_news_grid">
                        <div class="w3layouts_news_grid">
                            <img src="<?= base_url(SITETHEME) ?>images/3.jpg" alt=" " class="img-responsive" />
                            <div class="w3layouts_news_grid_pos">
                                <div class="wthree_text"><h3>plantation</h3></div>
                            </div>
                        </div>
                        <div class="agileits_w3layouts_news_grid">
                            <ul>
                                <li><i class="fa fa-calendar" aria-hidden="true"></i>25 July 2019</li>
                                <li><i class="fa fa-user" aria-hidden="true"></i><a href="#">Admin</a></li>
                            </ul>
                            <h4><a href="#" data-toggle="modal" data-target="#myModal">Design & Planting</a></h4>
                            <p>Row planting as applied in conventional horizontal farming or gardening is a system of growing crops in linear pattern in at least one direction rather than planting without any distinct arrangement.</p>
                        </div>
                    </div>
                    <div class="col-md-4 w3ls_news_grid">
                        <div class="w3layouts_news_grid">
                            <img src="<?= base_url(SITETHEME) ?>images/6.jpg" alt=" " class="img-responsive" />
                            <div class="w3layouts_news_grid_pos">
                                <div class="wthree_text"><h3>plantation</h3></div>
                            </div>
                        </div>
                        <div class="agileits_w3layouts_news_grid">
                            <ul>
                                <li><i class="fa fa-calendar" aria-hidden="true"></i>28 July 2019</li>
                                <li><i class="fa fa-user" aria-hidden="true"></i><a href="#">Admin</a></li>
                            </ul>
                            <h4><a href="#" data-toggle="modal" data-target="#myModal">Quality & Reliability</a></h4>
                            <p>the probability that a product, system, or service will perform its intended function adequately for a specified period of time, or will operate in a defined environment without failure.</p>
                        </div>
                    </div>
                    <div class="col-md-4 w3ls_news_grid">
                        <div class="w3layouts_news_grid">
                            <img src="<?= base_url(SITETHEME) ?>images/5.jpg" alt=" " class="img-responsive" />
                            <div class="w3layouts_news_grid_pos">
                                <div class="wthree_text"><h3>plantation</h3></div>
                            </div>
                        </div>
                        <div class="agileits_w3layouts_news_grid">
                            <ul>
                                <li><i class="fa fa-calendar" aria-hidden="true"></i>30 july 2019</li>
                                <li><i class="fa fa-user" aria-hidden="true"></i><a href="#">Admin</a></li>
                            </ul>
                            <h4><a href="#" data-toggle="modal" data-target="#myModal">Satisfied Customers</a></h4>
                            <p>It comes down to how our customer experiences the brand – and how that brand makes a person feel."</p>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!-- //news -->
        <!-- footer -->
        <?php $this->load->view('_footer') ?>
        <!-- //footer -->
        <!-- stats -->
        <script src="<?= base_url(SITETHEME) ?>js/jquery.waypoints.min.js"></script>
        <script src="<?= base_url(SITETHEME) ?>js/jquery.countup.js"></script>
        <script>
            $('.counter').countUp();
        </script>
        <!-- //stats -->
        <!-- mislider -->
        <script src="<?= base_url(SITETHEME) ?>js/mislider.js" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery(function ($) {
                var slider = $('.mis-stage').miSlider({
                    //  The height of the stage in px. Options: false or positive integer. false = height is calculated using maximum slide heights. Default: false
                    stageHeight: 380,
                    //  Number of slides visible at one time. Options: false or positive integer. false = Fit as many as possible.  Default: 1
                    slidesOnStage: false,
                    //  The location of the current slide on the stage. Options: 'left', 'right', 'center'. Defualt: 'left'
                    slidePosition: 'center',
                    //  The slide to start on. Options: 'beg', 'mid', 'end' or slide number starting at 1 - '1','2','3', etc. Defualt: 'beg'
                    slideStart: 'mid',
                    //  The relative percentage scaling factor of the current slide - other slides are scaled down. Options: positive number 100 or higher. 100 = No scaling. Defualt: 100
                    slideScaling: 150,
                    //  The vertical offset of the slide center as a percentage of slide height. Options:  positive or negative number. Neg value = up. Pos value = down. 0 = No offset. Default: 0
                    offsetV: -5,
                    //  Center slide contents vertically - Boolean. Default: false
                    centerV: true,
                    //  Opacity of the prev and next button navigation when not transitioning. Options: Number between 0 and 1. 0 (transparent) - 1 (opaque). Default: .5
                    navButtonsOpacity: 1,
                });
            });
        </script>
        <!-- //mislider -->
        <!-- text-effect -->
        <script type="text/javascript" src="<?= base_url(SITETHEME) ?>js/jquery.transit.js"></script>
        <script type="text/javascript" src="<?= base_url(SITETHEME) ?>js/jquery.textFx.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.test').textFx({
                    type: 'fadeIn',
                    iChar: 100,
                    iAnim: 1000
                });
            });
        </script>
        <!-- //text-effect -->
        <!-- menu -->
        <script>
            $(function () {

                initDropDowns($("div.shy-menu"));

            });

            function initDropDowns(allMenus) {

                allMenus.children(".shy-menu-hamburger").on("click", function () {

                    var thisTrigger = jQuery(this),
                            thisMenu = thisTrigger.parent(),
                            thisPanel = thisTrigger.next();

                    if (thisMenu.hasClass("is-open")) {

                        thisMenu.removeClass("is-open");

                    } else {

                        allMenus.removeClass("is-open");
                        thisMenu.addClass("is-open");
                        thisPanel.on("click", function (e) {
                            e.stopPropagation();
                        });
                    }

                    return false;
                });
            }
        </script>
        <!-- //menu -->
        <!-- start-smoth-scrolling -->
        <script type="text/javascript" src="<?= base_url(SITETHEME) ?>js/move-top.js"></script>
        <script type="text/javascript" src="<?= base_url(SITETHEME) ?>js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
        <!-- start-smoth-scrolling -->
        <!-- for bootstrap working -->
        <script src="<?= base_url(SITETHEME) ?>js/bootstrap.js"></script>
        <!-- //for bootstrap working -->
        <!-- here stars scrolling icon -->
        <script type="text/javascript">
            $(document).ready(function () {
                /*
                 var defaults = {
                 containerID: 'toTop', // fading element id
                 containerHoverID: 'toTopHover', // fading element hover id
                 scrollSpeed: 1200,
                 easingType: 'linear'
                 };
                 */

                $().UItoTop({easingType: 'easeOutQuart'});

            });
        </script>
        <!-- //here ends scrolling icon -->
        <script>
            $(document).ready(function () {
                $("#home").addClass(" active");
            });
        </script>
    </body>
</html>