<?php
$this->load->view('frontend/dashboard/_dash_header');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Ansonika">
        <title><?= $heading ?> - <?= SITETITLE ?></title>

        <!-- Favicons-->
        <link rel="shortcut icon" href="<?= base_url() . DASHTHEME ?>img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" type="image/x-icon" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-57x57-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-144x144-precomposed.png">

        <!-- Bootstrap core CSS-->
        <link href="<?= base_url() . DASHTHEME ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Main styles -->
        <link href="<?= base_url() . DASHTHEME ?>css/admin.css" rel="stylesheet">
        <!-- Icon fonts-->
        <link href="<?= base_url() . DASHTHEME ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Plugin styles -->
        <link href="<?= base_url() . DASHTHEME ?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
        <!-- Your custom styles -->
        <link href="<?= base_url() . DASHTHEME ?>css/custom.css" rel="stylesheet">
        <!-- Date Picker -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <style>
            .suggestion-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;z-index: 99999;}
            .suggestion-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
            .suggestion-list li:hover{background:#ece3d2;cursor: pointer;}
        </style>
    </head>

    <body class="fixed-nav sticky-footer" id="page-top">
        <?php
        $this->load->view('frontend/dashboard/_dash_nav');
        ?>
        <!-- /Navigation-->
        <div class="content-wrapper">
            <form id="single_basic" method="POST" novalidate="novalidate" enctype="multipart/form-data">
                <div class="container-fluid">
                    <form id="single_basic" method="POST" novalidate="novalidate" enctype="multipart/form-data">
                        <input type="hidden" name="building_id" id="building_id" value="<?= __echo($propertyData, 'building_id') ?>">
                        <input type="hidden" name="unit_hashval" id="unit_hashval" value="<?= __echo($propertyData, 'unit_hashval') ?>">
                        <input type="hidden" name="partner_hashval" id="partner_hashval" value="<?= __echo($partnerData, 'partner_hashval') ?>">
                        <!-- Breadcrumbs-->
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<?= base_url('dashboard') ?>">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active"><?= $heading ?></li>
                        </ol>
                        <div class="box_general padding_bottom">
                            <div class="header_box version_2">
                                <h2><i class="fa fa-file"></i>Basic info</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City Name</label>
                                        <input type="text" class="form-control" value="<?= __echo($propertyData, 'city_name') ?>" name="city_name" id="city_name" placeholder="City Name" autocomplete="off">
                                        <input type="hidden" name="c_id" id="c_id" value="<?= __echo($propertyData, 'city_id') ?>">
                                        <div id="suggesstion-box-city"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Neighbourhood</label>
                                        <input type="text" value="<?= __echo($propertyData, 'neighbourhood_name') ?>" name="neighbourhood_name" id="neighbourhood_name" class="form-control" placeholder="Neighbourhood Name" autocomplete="off">
                                        <div id="suggesstion-box"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Unit Type</label>
                                        <input type="hidden" value="1" name="place_type" id="place_type">
                                        <select class="styled-select form-control" name="place_sub_type" id="place_sub_type">
                                            <option value=0>Select</option>
                                            <option value="1" <?php if ($propertyData['place_sub_type'] == 1) echo 'selected'; ?>>Town House</option>
                                            <option value="2" <?php if ($propertyData['place_sub_type'] == 2) echo 'selected'; ?>>House</option>
                                            <option value="3" <?php if ($propertyData['place_sub_type'] == 3) echo 'selected'; ?>>Apartment</option>
                                            <option value="4" <?php if ($propertyData['place_sub_type'] == 4) echo 'selected'; ?>>Duplex/Triplex/Multiplex</option>
                                            <option value="5" <?php if ($propertyData['place_sub_type'] == 5) echo 'selected'; ?>>Basement</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <div class="editor"></div>
                                        <textarea class="form-control" name="building_desc" id="building_desc" placeholder="Building Description"><?= __echo($propertyData, 'building_desc') ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="header_box version_2">
                                <h2><i class="icon-meh"></i>Location</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" value="<?= __echo($propertyData, 'addr_placeholder') ?>" name="input_address" id="input_address" class="form-control" placeholder="Address">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Street</label>
                                        <input type="text" value="<?= __echo($propertyData, 'addr_street') ?>" name="street_number" id="street_number" class="form-control" placeholder="Street Number">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Area</label>
                                        <input type="text" value="<?= __echo($propertyData, 'addr_area') ?>" name="route" id="route" class="form-control" placeholder="Area">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="text" value="<?= __echo($propertyData, 'city_name') ?>" name="locality" id="locality" class="form-control" placeholder="City">
                                        <input type="hidden" name="administrative_area_level_1" id="administrative_area_level_1" value="<?= __echo($propertyData, 'state_iso') ?>">
                                        <input type="hidden" name="country" id="country" value="<?= __echo($propertyData, 'country_name') ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Postal / Zip Code</label>
                                        <input type="text" value="<?= __echo($propertyData, 'postalcode') ?>" name="postal_code" id="postal_code" class="form-control" placeholder="Postal / Zip Code">
                                        <input value="<?= __echo($propertyData, 'lati') ?>" name="lati" id="lati" type="hidden">
                                        <input value="<?= __echo($propertyData, 'longi') ?>" name="longi" id="longi" type="hidden">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div id="map" style="max-height:400px !important;"></div>
                                        <div id="infowindow-content">
                                            <img src="" name="place-icon" id="place-icon">
                                            <span id="place-name"  class="title"></span><br>
                                            <span id="place-address"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="header_box version_2">
                                <h2><i class="icon-meh"></i>Contact Details</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" value="<?= __echo($propertyData, 'contact_person') ?>" name="contact_person" id="contact_person" class="form-control" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" value="<?= __echo($propertyData, 'email') ?>" name="email" id="email" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="number" value="<?= __echo($propertyData, 'phone') ?>" name="phone" id="phone" class="form-control" placeholder="Phone">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="header_box version_2">
                                <h2><i class="icon-meh"></i>Unit Details</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Unit Name / Number</label>
                                        <input type="text" value="<?= __echo($propertyData, 'unit_name') ?>" name="unit_name" id="unit_name" class="form-control" placeholder="Unit Name / Number">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Rent</label>
                                        <input type="number" value="<?= __echo($propertyData, 'price') ?>" name="price" id="price" class="form-control" placeholder="CAD">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Area Sqft.</label>
                                        <input type="number" value="<?= __echo($propertyData, 'sqft') ?>" name="sqft" id="sqft" class="form-control" placeholder="SqFt">
                                    </div>
                                </div>
                            </div>
                            <!-- /row-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bedrooms</label>
                                        <input type="number" value="<?= __echo($propertyData, 'bedrooms') ?>" name="bedrooms" id="bedrooms" class="form-control" placeholder="Bedrooms">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bathroom</label>
                                        <input type="number" value="<?= __echo($propertyData, 'washrooms') ?>" name="washrooms" id="washrooms" class="form-control" placeholder="Bathrooms">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Smoking</label>
                                        <select class="styled-select form-control" name="smoking" name="smoking">
                                            <option value="0">Select</option>
                                            <option value="1" <?php if ($propertyData['smoking'] == 1) echo 'selected'; ?>>Not Allowed</option>
                                            <option value="2" <?php if ($propertyData['smoking'] == 2) echo 'selected'; ?>>Allowed</option>
                                            <option value="3" <?php if ($propertyData['smoking'] == 3) echo 'selected'; ?>>Outdoors Only</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Pets</label>
                                        <select class="styled-select form-control" name="pets" id="pets">
                                            <option value="0" <?php if ($propertyData['pets'] == 0) echo 'selected'; ?>>Not Allowed</option>
                                            <option value="1" <?php if ($propertyData['pets'] == 1) echo 'selected'; ?>>Conditionally Allowed</option>
                                            <option value="2" <?php if ($propertyData['pets'] == 2) echo 'selected'; ?>>Allowed</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Parking (Including Garage)</label>
                                        <input type="number" value="<?= __echo($propertyData, 'parking') ?>" name="parking" id="parking" class="form-control" placeholder="Parking">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Available From</label>
                                        <?php
                                        $from_date = isset($propertyData['from_date']) ? $propertyData['from_date'] : date('d-M-Y');
                                        ?>
                                        <input type="text" value="<?= date('d-M-Y', strtotime($from_date)) ?>" name="from_date" id="from_date" class="form-control" placeholder="Move in Date" readonly="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Agreement Type</label>
                                        <select class="styled-select form-control" name="agreement_type" name="agreement_type">
                                            <option value="0" <?php if ($propertyData['agreement_type'] == 0) echo 'selected'; ?>>Year Lease</option>
                                            <option value="1" <?php if ($propertyData['agreement_type'] == 1) echo 'selected'; ?>>Month to Month</option>
                                            <option value="2" <?php if ($propertyData['agreement_type'] == 2) echo 'selected'; ?>>6 Months Lease</option>
                                            <option value="3" <?php if ($propertyData['agreement_type'] == 3) echo 'selected'; ?>>Short Term</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="header_box version_2">
                                <h2>Building Features</h2>
                            </div>
                            <div class="row">
                                <?php
                                foreach ($features_included as $included) {
                                    $inc[] = $included['amenity_id'];
                                }
                                if (empty($inc)) {
                                    $inc = array();
                                }
                                foreach ($features as $feature) {
                                    $checked = '';
                                    if (in_array($feature['id'], $inc)) {
                                        $checked = 'checked';
                                    }
                                    ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input class="container_check" type="checkbox" value="<?= $feature['id'] ?>" name="features[]" id="features_<?= $feature['id'] ?>" <?= $checked ?>>
                                            <label class="container_check" for="features_<?= $feature['id'] ?>">
                                                &nbsp; &nbsp; &nbsp; <?= $feature['amenity'] ?>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <hr>
                            <div class="header_box version_2">
                                <h2><i class="icon-meh"></i>Appliances</h2>
                            </div>
                            <div class="row">
                                <?php
                                foreach ($appliances_included as $included) {
                                    $inc[] = $included['amenity_id'];
                                }
                                if (empty($inc)) {
                                    $inc = array();
                                }
                                foreach ($appliances as $appliance) {
                                    $checked = '';
                                    if (in_array($appliance['id'], $inc)) {
                                        $checked = 'checked';
                                    }
                                    ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input class="container_check" type="checkbox" type="checkbox" value="<?= $appliance['id'] ?>" name="appliances[]" id="appliances_<?= $appliance['id'] ?>" <?= $checked ?>>
                                            <label class="form-check-label" for="appliances_<?= $appliance['id'] ?>">
                                                <?= $appliance['amenity'] ?>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <hr>
                            <div class="header_box version_2">
                                <h2><i class="icon-meh"></i>Utilities (Included)</h2>
                            </div>
                            <div class="row">
                                <?php
                                foreach ($utilities_included as $included) {
                                    $inc[] = $included['amenity_id'];
                                }
                                if (empty($inc)) {
                                    $inc = array();
                                }
                                foreach ($utilities as $utility) {
                                    $checked = '';
                                    if (in_array($utility['id'], $inc)) {
                                        $checked = 'checked';
                                    }
                                    ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input class="container_check" type="checkbox" type="checkbox" value="<?= $utility['id'] ?>" name="utilities_inc[]" id="utilities_inc_<?= $utility['id'] ?>" <?= $checked ?>>
                                            <label class="form-check-label" for="utilities_inc_<?= $utility['id'] ?>">
                                                <?= $utility['amenity'] ?>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <hr>
                            <div class="header_box version_2">
                                <h2><i class="icon-meh"></i>Utilities - Not Included</h2>
                            </div>
                            <div class="row">
                                <?php
                                foreach ($utilities_notincluded as $notincluded) {
                                    $notinc[] = $notincluded['amenity_id'];
                                }
                                if (empty($notinc)) {
                                    $notinc = array();
                                }
                                foreach ($utilities as $utility) {
                                    $checked = '';
                                    if (in_array($utility['id'], $notinc)) {
                                        $checked = 'checked';
                                    }
                                    ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input class="container_check" type="checkbox" type="checkbox" value="<?= $utility['id'] ?>" name="paid_utilities[]" id="paid_utilities_<?= $utility['id'] ?>" <?= $checked ?>>
                                            <label class="container_check" for="paid_utilities_<?= $utility['id'] ?>">
                                                &nbsp; &nbsp; &nbsp; <?= $utility['amenity'] ?>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <hr>
                            <div class="header_box version_2">
                                <h2><i class="icon-meh"></i>Photos</h2>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-group">
                                        <input type="file" name="photos[]" id="photos" class="form-control" multiple>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php
                                    foreach ($photoData as $pData) {
                                        $photo = base_url() . UPLOAD_DIR . ENTITY_IMAGE_DIR . THUMB_DIR . $pData['path'];
                                        ?>
                                        <div style="float:left;border:1px solid; padding: 5px;margin-right: 5px;">
                                            <img src="<?= $photo ?>" alt="Entity Photo" title="Entity Photo" width="70"><br />
                                            <a href="javascript:void(0);" onclick="javascript:delete_photo('<?= $pData['id'] ?>', '<?= $pData['unit_id'] ?>');" style="color:red;">
                                                <span><strong>X</strong></span>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="status_msg"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn_1 medium pull-right">Save Property Details</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.container-fluid-->
        </div>

        <?php
        $this->load->view('frontend/dashboard/_dash_footer');
        ?>
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <!-- Bootstrap core JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Page level plugin JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/chart.js/Chart.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/datatables/jquery.dataTables.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/datatables/dataTables.bootstrap4.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery.selectbox-0.2.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/retina-replace.min.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery.magnific-popup.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="<?= base_url() . DASHTHEME ?>js/admin.js"></script>
        <script type="text/javascript" src="<?= base_url() . COMMONTHEME ?>js/my_script.js"></script>
        <!-- Date Picker-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    </body>
    <script>
                                            $(document).ready(function () {
                                                $("#dash_add_my_properties").addClass(" active");
                                            });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#from_date').datepicker({
                format: 'dd-M-yyyy',
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startDate: new Date(),
            });
        });

        // AJAX call for neighbourhood
        $(document).ready(function () {
            $("#city_name").keyup(function () {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url() ?>entity/get_cities",
                    data: {
                        keyword: $(this).val()
                    },
                    success: function (data) {
                        $("#suggesstion-box-city").show();
                        $("#suggesstion-box-city").html(data);
                        $("#city_name").css("background", "#FFF");
                    }
                });
            });
        });
        //To select City
        function selectCity(city_name, city_id) {
            $("#city_name").val(city_name);
            $("#c_id").val(city_id);
            $("#suggesstion-box-city").hide();
        }

        // AJAX call for neighbourhood
        $(document).ready(function () {
            $("#neighbourhood_name").keyup(function () {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url() ?>entity/get_neighbourhoods",
                    data: {
                        city_id: $("#c_id").val(),
                        keyword: $(this).val()
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#neighbourhood_name").css("background", "#FFF");
                    }
                });
            });
        });
        //To select Neighbourhood
        function selectNeighbourhood(val) {
            $("#neighbourhood_name").val(val);
            $("#suggesstion-box").hide();
        }
    </script>
    <script>
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 52.1332144, lng: -106.6700458}, // Saskatoon lat long
                zoom: 13
            });
            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });

            var input = document.getElementById('input_address');
            var autocomplete = new google.maps.places.Autocomplete(input);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            autocomplete.addListener('place_changed', function () {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'long_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                // Clear the inputs before adding new address
                for (var component in componentForm) {
                    document.getElementById(component).value = '';
                }
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        document.getElementById(addressType).value = val;
                    }
                }

                //document.getElementById('place_id').value = place.place_id
                document.getElementById('lati').value = place.geometry.location.lat()
                document.getElementById('longi').value = place.geometry.location.lng()

                infowindowContent.children['place-icon'].src = place.icon;
                infowindowContent.children['place-name'].textContent = place.name;
                infowindowContent.children['place-address'].textContent = address;
                infowindow.open(map, marker);
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_MAPS_API_EMBED ?>&libraries=places&callback=initMap"
    async defer></script>
    <script>
        $(document).ready(function () {
            $('#single_basic').validate({
                rules: {
                    city_name: {required: true},
                    place_type: {required: true},
                    place_sub_type: {required: true},
                    price: {required: true},
                    bedrooms: {required: true},
                    washrooms: {required: true},
                    parking: {required: true},
                    from_date: {required: true},
                    building_desc: {required: true},
                    input_address: {required: true, minlength: 3},
                    locality: {required: true, minlength: 2}, // For City
                    administrative_area_level_1: {required: true}, // For State / Province
                    country: {required: true},
                    postal_code: {required: true},
                    place_id: {required: true},
                    lati: {required: true},
                    longi: {required: true},
                    contact_person: {required: true},
                    email: {required: true},
                    phone: {required: true}
                },
                highlight: function (element, errorClass) {
                    $(element).css({borderColor: '#FF0000'});
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).css({borderColor: '#CCCCCC'});
                },
                errorPlacement: function (error, element) {
                    $.validator.messages.required = '';
                },
                //messages: {email: "Please enter a valid email address"},
                //invalidHandler: function(form, validator) {},
                submitHandler: function (form) {
                    //showprocess();
                    var formData = new FormData($('#single_basic')[0]);
                    $.ajax({
                        url: '<?= base_url() ?>entity/addupdate_single_property',
                        type: 'POST',
                        data: formData,
                        async: false,
                        processData: false, // tell jQuery not to process the data
                        contentType: false, // tell jQuery not to set contentType
                        success: function (data) {
                            //removeprocess();
                            try {
                                var response = jQuery.parseJSON(data);
                                if (response.status != 'error') {
                                    $('#single_basic .status_msg').html(getSuccessHTML(response.msg));
                                    //windowRedirect(response.redirect, 1000);
                                    location.reload();
                                } else {
                                    $('#single_basic .status_msg').html(getErrorHTML(response.msg));
                                }
                            } catch (err) {
                                $('#single_basic .status_msg').html(getErrorHTML(AJAX_ERR));
                            }

                        }
                    });
                    return false;
                }
            });
        });
    </script>
    <script>
        function delete_photo(photo_id, unit_id) {
            showprocess();
            var answer = confirm("Are you want to delete the photo? You can't revert this action!");
            if (answer) {
                $.ajax({
                    type: "POST",
                    url: '<?= base_url() ?>entity/delete_entity_photo',
                    data: {
                        photo_id: photo_id,
                        unit_id: unit_id
                    },
                    success: function (data) {
                        removeprocess();
                        try {
                            var response = jQuery.parseJSON(data);
                            if (response.status == 'success') {
                                $('#single_basic .status_msg').html(getSuccessHTML(response.msg));
                                windowRedirect(response.redirect, 1000);
                                location.reload();
                            } else {
                                $('#single_basic .status_msg').html(getErrorHTML(response.msg));
                            }
                        } catch (err) {
                            $('#single_basic .status_msg').html(getErrorHTML(AJAX_ERR));
                        }
                    }
                });
            }
        }
    </script>
</html>
