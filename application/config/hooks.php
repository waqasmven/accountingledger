<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | Hooks
  | -------------------------------------------------------------------------
  | This file lets you define "hooks" to extend CI without hacking the core
  | files.  Please see the user guide for info:
  |
  |	https://codeigniter.com/user_guide/general/hooks.html
  |
 */

$hook['post_controller_constructor'][] = array(
    'class' => 'Metadata',
    'function' => 'get_metadata',
    'filename' => 'Metadata.php',
    'filepath' => 'hooks'
);

if (ENVIRONMENT == 'production') {
// Compress HTML output
    $hook['display_override'][] = array(
        'class' => '',
        'function' => 'compress',
        'filename' => 'Compress.php',
        'filepath' => 'hooks'
    );
}