<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once dirname(__FILE__) . '/PHPMailer/PHPMailerAutoload.php';

class Myphpmailer {

    private $CI;

    function __construct($params = array()) {
        $this->CI = &get_instance();
    }

    public function sendsmtpemail($array) {
        $errors = '';
        if (empty($errors)) {
            $mail = new PHPMailer(true); //defaults to using php "mail()"; the true param means it will throw exceptions on errors, which we need to catch
            try {
                if (isset($array['from_email']) && $array['from_email'] != '')
                    $f_email = $array['from_email'];
                else
                    $f_email = FROMEMAIL;
                $mail->SMTPDebug = 0; // Enable verbose debug output
                $mail->isSMTP(); // Set mailer to use SMTP
                $mail->Host = SMTPHOST; // Specify main and backup SMTP servers
                $mail->SMTPAuth = true; // Enable SMTP authentication
                $mail->Username = SMTPUSERNAME; // SMTP username
                $mail->Password = SMTPPASSWORD; // SMTP password
                $mail->SMTPSecure = SMTPSECURE; // Enable TLS encryption, `ssl` also accepted
                $mail->Port = SMTPPORT; // TCP port to connect, tls=587, ssl=465
                $mail->From = $f_email;
                $mail->FromName = SITETITLE;
                $mail->AddEmbeddedImage("rocks.png", "venuetop-logo", "rocks.png");
                $mail->WordWrap = 50; // Set word wrap to 50 characters
                $mail->isHTML(true); // Set email format to HTML
                //
                // Set Params
                $mail->addAddress($array['email_to'], '');     // Add a recipient
                $mail->addReplyTo($array['email_to'], '');
                if (isset($array['email_cc']) && $array['email_cc'] != '') {
                    $mail->addCC($array['email_cc'], ''); //Set CC address
                } else {
                    $mail->addCC(INFO_EMAIL, ''); //Set CC address
                }
                $mail->Subject = $array['email_subject'];
                $mail->Body = $array['email_body'];
                //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                if (isset($array['email_attach']) && $array['email_attach'] != '') {
                    $mail->addAttachment('images/phpmailer_mini.png');
                }

                if (!$mail->send()) {
                    return 0;
                } else {
                    return 1;
                }
                $errors[] = "Send mail sucsessfully";
            } catch (phpmailerException $e) {
                $errors[] = $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                $errors[] = $e->getMessage(); //Boring error messages from anything else!
            }
        }
    }

    public function sendemail($array) {
        $to = $array['email_to'];
        $subject = $array['email_subject'];
        $message = $array['email_body'];
        if (isset($array['email_cc']) && $array['email_cc'] != '') {
            $cc = $array['email_cc'];
        } else {
            $cc = INFO_EMAIL;
        }

        if (isset($array['from_email']) && $array['from_email'] != '')
            $f_email = $array['from_email'];
        else
            $f_email = FROMEMAIL;

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From:' . SITETITLE . ' <' . $f_email . '>' . "\r\n";
        $headers .= 'Cc: ' . $cc . "\r\n";

        if (mail($to, $subject, $message, $headers)) {
            return 1;
        } else {
            return 0;
        }
    }

}
