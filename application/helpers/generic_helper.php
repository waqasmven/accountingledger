<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package        CodeIgniter
 * @author        Sunil Khandade
 * @copyright           Copyright (c) 2016, Tenans.com
 * @since        Version 1.0
 */
if (!function_exists('echo_')) {

    function echo_(&$var, $empty_str = '') {
        if (isset($var)) {
            echo $var;
        } else {
            echo $empty_str;
        }
    }

}
if (!function_exists('log_activity')) {

    function log_activity($by_id, $module, $activity, $data_array) {
        $ci = &get_instance();
        $ci->load->model('common/Generic_model');
        $ci->Generic_model->activity_log(array(
            'modified_by_id' => $by_id,
            'module' => $module,
            'activity' => $activity,
            'data' => $data_array
        ));
    }

}
if (!function_exists('get_session_user_id')) {

    function get_session_user_id($default = null) {
        $ci = &get_instance();
        return strictEmpty($ci->session->userdata('user_id')) ?
                (strictEmpty($ci->session->userdata('partner_id')) ?
                strictEmpty($ci->session->userdata('realnet_user_id')) ? $default : $ci->session->userdata('realnet_user_id') : ($ci->session->userdata('partner_id'))) : ($ci->session->userdata('user_id'));
    }

}

if (!function_exists('get_session_user_name')) {

    function get_session_user_name($default = null) {
        $ci = &get_instance();
        return $ci->session->userdata('Fname');
        /* return strictEmpty($ci->session->userdata('user_id')) ?
          (strictEmpty($ci->session->userdata('partner_id')) ?
          strictEmpty($ci->session->userdata('realnet_user_id')) ? $default : $ci->session->userdata('realnet_user_id') : ($ci->session->userdata('partner_id'))) : ($ci->session->userdata('user_id'));
         */
    }

}

if (!function_exists('get_session_user_pic')) {

    function get_session_user_pic($default = null) {
        $ci = &get_instance();
        return $ci->session->userdata('P_pic');
        /* return strictEmpty($ci->session->userdata('user_id')) ?
          (strictEmpty($ci->session->userdata('partner_id')) ?
          strictEmpty($ci->session->userdata('realnet_user_id')) ? $default : $ci->session->userdata('realnet_user_id') : ($ci->session->userdata('partner_id'))) : ($ci->session->userdata('user_id'));
         */
    }

}

if (!function_exists('get_session_user_type')) {

    function get_session_user_type($default = null) {
        $ci = &get_instance();
        return strictEmpty($ci->session->userdata('user_type')) ? (strictEmpty($ci->session->userdata('realnet_user_type')) ? $ci->session->userdata('partner_types') : $ci->session->userdata('realnet_user_type')) : ($ci->session->userdata('user_type'));
    }

}

if (!function_exists('get_session_email')) {

    function get_session_email() {
        $ci = &get_instance();
        return strictEmpty($ci->session->userdata('front_email')) ? (strictEmpty($ci->session->userdata('partner_email')) ? $ci->session->userdata('realnet_email') : $ci->session->userdata('partner_email')) : $ci->session->userdata('front_email');
    }

}

if (!function_exists('getIP')) {

    function getIP() {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED',
    'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }

}

if (!function_exists('get_post_input')) {

    function get_post_input($input) {
        return $this->input->post($input);
    }

}
/**
 * Generate Random Md5 Hash
 */
if (!function_exists('get_random_md5')) {

    function get_random_md5($id = '') {
        return md5($id . rand(0000000001, 2147483647) . time());
    }

}

if (!function_exists('send_mail')) {

    function send_mail($params = array()) {
        $CI = &get_instance();
        $CI->load->library('myphpmailer');
        // Setting up logo URL
        $logo_path = base_url() . SITETHEME . 'img/logos/venuetop-logo.png';
        $params['email_body'] = str_replace("[ER_LOGO]", $logo_path, $params['email_body']);
        try {
            if ($_SERVER['HTTP_HOST'] == 'localhost') {
                $result = $CI->myphpmailer->sendsmtpemail($params);
            } else {
                $result = $CI->myphpmailer->sendemail($params);
            }
            //print_r($result);die;
            if ($result) {
                $CI->load->model('common/Generic_model');
                $cc = isset($params['email_cc']) ? $params['email_cc'] : INFO_EMAIL;
                $CI->Generic_model->email_log(array(
                    'email_type' => $params['email_type'],
                    'email_to' => $params['email_to'],
                    'email_cc' => $cc, // Checking if it is null
                    'email_subject' => $params['email_subject']
                ));
                return true;
            }
        } catch (Exception $e) {
            log_message('error', 'Send Mail Failed ' + $e);
            return false;
        }
    }

}

/* if (!function_exists('send_mail')) {

  function send_mail($params = array()) {
  $CI = &get_instance();
  $CI->load->library('myphpmailer');
  // Setting up logo URL
  $logo_path = base_url() . SITETHEME . 'img/logos/venuetop-logo.png';
  $params['email_body'] = str_replace("[ER_LOGO]", $logo_path, $params['email_body']);
  try {
  $email = $params['email_to'];
  $name = 'There';
  $body = $params['email_body'];
  $subject = $params['email_subject'];
  $headers = array(
  "Authorization: Bearer ".SENDGRID_API_KEY,
  'Content-Type: application/json'
  );
  $data = array(
  'personalizations' => array(
  array(
  'to' => array(
  array(
  'email' => $email,
  'name' => $name
  )
  )
  )
  ),
  'from' => array(
  'email' => 'no-reply@vanuetop.com',
  'name' => 'Tenans.com'
  ),
  'subject' => $subject,
  'content' => array(
  array(
  'type' => 'text/html',
  'value' => $body
  )
  )
  );

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $response = curl_exec($ch);
  curl_close($ch);

  // Log Email details
  $CI->load->model('common/Generic_model');
  $cc = isset($params['email_cc']) ? $params['email_cc'] : INFO_EMAIL;
  $CI->Generic_model->email_log(array(
  'email_type' => $params['email_type'],
  'email_to' => $params['email_to'],
  'email_cc' => $cc, // Checking if it is null
  'email_subject' => $params['email_subject']
  ));
  } catch (Exception $e) {
  log_message('error', 'Send Mail Failed ' + $e);
  return false;
  }
  }
  } */

if (!function_exists('set_db_cart')) {

    function set_db_cart($entity_id) {
        $CI = &get_instance();  //get instance, access the CI superobject
        $CI->load->model('Entity_model');
        if ($entity_id > 0) {
            $CI->Entity_model->set_entity_cart($entity_id);
        }
    }

}

if (!function_exists('remove_db_cart')) {

    function remove_db_cart($entity_id) {
        $CI = &get_instance();  //get instance, access the CI superobject
        $CI->load->model('Entity_model');
        if ($entity_id > 0) {
            $CI->Entity_model->remove_entity_cart($entity_id);
        }
    }

}

if (!function_exists('update_db_cart')) {

    function update_db_cart($new_status, $entity_id, $conn = null) {
        $CI = &get_instance();  //get instance, access the CI superobject
        $CI->load->model('Entity_model');
        $CI->Entity_model->update_entity_cart($new_status, $entity_id, $conn);
    }

}

if (!function_exists('set_or_update_user_cart')) {

    function set_or_update_user_cart(& $CI, $user_id) {
        $CI->load->model('common/Entity_model');
        $sess_cart = $CI->session->cart;
        $db_cart = $CI->Entity_model->get_entity_cart($user_id);

        if ($sess_cart != null) {
            foreach ($sess_cart as $sess_cart_item) {
                set_db_cart($sess_cart_item['entity_id']);
            }
        }
        foreach ($db_cart as $db_cart_item) {
            $sess_cart = $CI->session->cart;
            if (count($sess_cart) <= Config::MAX_CART) {
                session_cart_add($CI, $sess_cart, $db_cart_item['entity_id'], 'entity_id');
            }
        }
    }

}

if (!function_exists('set_success_message')) {

    function set_success_message($msg) {
        $CI = &get_instance();  //get instance, access the CI superobject
        $html = '<div class="alert alert-success">
                <button class="close" type="button" data-dismiss="alert">
                <span aria-hidden="true">×</span></button>
                <p class="text-bigger">' . $msg . '</p>
            </div>';
        $CI->session->set_flashdata('session_success_msg', $html);
        return $html;
    }

}


if (!function_exists('set_realnet_session_after_login')) {

    function set_realnet_session_after_login($result) {
        $CI = &get_instance();  //get instance, access the CI superobject
        $CI->session->set_userdata('user_id', $result['u_id']);
        $CI->session->set_userdata('username', $result['username']);
        $CI->session->set_userdata('email', $result['email']);
        $CI->session->set_userdata('P_pic', $result['p_pic']);
        $CI->session->set_userdata('Fname', $result['Full_Name']);
        $CI->session->set_userdata('user_type', $result['type']);
        /* $CI->session->set_userdata('realnet_user_hashval', $result['hashval']);
          //$CI->session->set_userdata('realnet_user_type', $result['user_type']);
          //$CI->session->set_userdata('realnet_user_sla', $result['sla']);
          if ($result['user_type'] == 'ADMIN') {
          $CI->session->set_userdata('admin_login', $result['hashval']);
          }
          $CI->session->set_userdata('realnet_user_status', $result['status']);

         */
    }

}

if (!function_exists('set_success_msg')) {

    function set_success_msg() {
        $CI = &get_instance();  //get instance, access the CI superobject
        $CI->session->set_userdata('msg', 'Data has been updated');
    }

}

if (!function_exists('get_success_msg')) {

    function get_success_msg($default = null) {
        $ci = &get_instance();
        return strictEmpty($ci->session->userdata('msg'));
    }

}

if (!function_exists('set_partner_session_after_login')) {

    function set_partner_session_after_login(&$ci, $result) {
        $ci->session->unset_userdata('search_form');

        $ci->session->set_userdata('partner_email', $result['email']);
        $ci->session->set_userdata('partner_id', $result['id']);
        $ci->session->set_userdata('partner_fullname', $result['fname'] . " " . $result['lname']);
        $ci->session->set_userdata('partner_thumbphoto', ($result['photo'] != '') ? base_url() . UPLOAD_DIR . PROFILE_IMAGE_DIR .
                        $result['id'] . DIR_SEP . THUMB_DIR . $result['photo'] : (base_url() . UPLOAD_DIR .
                        DEFAULT_DIR . PROFILE_IMAGE_DIR . THUMB_DIR . DEFAULT_PROFILE_IMAGE));
        $ci->session->set_userdata('partner_status', $result['status']);
        $ci->session->set_userdata('partner_type', $result['user_type']);
        $ci->session->set_userdata('partner_logintime', date('Y-m-d h:i:s'));

        $ci->load->model('common/Entity_model');
        $partner_property = $ci->Entity_model->get_partner_entity_after_login($result['id']);
        $partner_property = array_column_multidimensional($partner_property, 'sla_check');
        $active_entity = isset($partner_property[1]) ? array_column($partner_property[1], 'id') : array();
        $sla_check_entity = isset($partner_property[0]) ? array_column($partner_property[0], 'id') : array();
        $total_entity = array_merge($active_entity, $sla_check_entity);
        $ci->session->set_userdata('active_entity', $active_entity);
        $ci->session->set_userdata('sla_check_entity', $sla_check_entity);
        $ci->session->set_userdata('total_entity_list', $total_entity);
    }

}


if (!function_exists('set_user_session_after_login')) {

    function set_user_session_after_login(&$ci, $result) {
        $ci->session->set_userdata('user_email', $result['email']);
        $ci->session->set_userdata('user_id', $result['id']);
        $ci->session->set_userdata('user_hashval', $result['hashval']);
        $ci->session->set_userdata('user_fname', $result['fname']);
        $ci->session->set_userdata('user_lname', $result['lname']);
        $ci->session->set_userdata('user_fullname', $result['fname'] . " " . $result['lname']);
        $ci->session->set_userdata('user_thumbphoto', ($result['photo'] != '') ? base_url() . UPLOAD_DIR . PROFILE_IMAGE_DIR .
                        $result['id'] . DIR_SEP . THUMB_DIR . $result['photo'] : (base_url() . UPLOAD_DIR .
                        DEFAULT_DIR . PROFILE_IMAGE_DIR . THUMB_DIR . DEFAULT_PROFILE_IMAGE));
        $ci->session->set_userdata('user_status', $result['status']);
        $ci->session->set_userdata('user_types', $result['user_type']);
        $ci->session->set_userdata('user_logintime', date('Y-m-d h:i:s'));
        $ci->session->set_userdata('show_search', true);
        set_or_update_user_cart($ci, $result['id']);
    }

}

if (!function_exists('set_info_message')) {

    function set_info_message($msg) {
        $CI = &get_instance();  //get instance, access the CI superobject
        $html = '<div class="alert alert-info">
                <button class="close" type="button" data-dismiss="alert">
                <span aria-hidden="true">×</span></button>
                <p class="text-bigger">' . $msg . '</p>
            </div>';
        $CI->session->set_flashdata('session_info_msg', $html);
        return $html;
    }

}

if (!function_exists('set_error_message')) {

    function set_error_message($msg) {
        $CI = &get_instance();  //get instance, access the CI superobject
        $html = '<div class="alert alert-danger">
                <button class="close" type="button" data-dismiss="alert">
                <span aria-hidden="true">×</span></button>
                <p class="text-bigger">' . $msg . '</p>
            </div>';
        $CI->session->set_flashdata('session_error_msg', $html);
        return $html;
    }

}

if (!function_exists("verify_captcha")) { // use ReCaptcha.php library

    function verify_captcha($a) {
        
    }

}

if (!function_exists("strictEmpty")) { // use ReCaptcha.php library

    function strictEmpty($var) {
        // Delete this line if you want space(s) to count as not empty
        $var = trim($var);
        if (isset($var) === true && $var === '') {
            return true;
        } else {
            return false;
        }
    }

}

if (!function_exists('reload_captcha')) { // Old Captcha

    function reload_captcha() {
        $CI = &get_instance();
        $CI->load->helper('captcha');
        $vals = array(
            'img_path' => 'captcha/',
            'img_url' => base_url() . 'captcha/',
            'img_width' => '150',
            'img_height' => 30,
            'expiration' => 200,
            'word_length' => 5,
            'font_size' => 36,
            'img_id' => 'Imageid',
            'pool' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        );
        $cap = create_captcha($vals);
        $CI->session->set_userdata(array(
            'captchasecurity' => $cap['word']
        ));
        return $cap['image'];
    }

}

if (!function_exists('get_lat_long_by_city')) {

    function get_lat_long_by_city($city, $state, $country) {
        $CI = &get_instance();
        $CI->load->model('Generic_model');
        $lati_longi = $CI->Generic_model->get_city_lat_long($city, $state, $country);
        if ($lati_longi['city_lati'] !== null && $lati_longi['city_longi'] !== null) {
            return $lati_longi;
        }
        $lati_longi1 = get_google_lat_long_by_city($city, $state, $country);
        $CI->Generic_model->update_city_lat_long($lati_longi1['city_lati'], $lati_longi1['city_longi'], $lati_longi['city_id']);
        return $lati_longi1;
    }

}

if (!function_exists('get_google_lat_long_by_city')) {

    function get_google_lat_long_by_city($city, $state, $country) {
        $data_arr = array('city_lati' => null, 'city_longi' => null);
        $CI = &get_instance();
        $address = str_replace(" ", "-", $city) . '-' . $state . '-' . $country;
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=" . GOOGLE_MAPS_API_LAT_LONG;
        $json = file_get_contents($url);
        $resp = json_decode($json, true);
        // response status will be 'OK', if able to geocode given address
        if ($resp['status'] == 'OK') {
            // get the important data
            $lati = isset($resp['results'][0]['geometry']['location']['lat']) ? $resp['results'][0]['geometry']['location']['lat'] : "";
            $longi = isset($resp['results'][0]['geometry']['location']['lng']) ? $resp['results'][0]['geometry']['location']['lng'] : "";
            // verify if data is complete
            if ($lati && $longi) {
                // put the data in the array
                $data_arr['city_lati'] = $lati;
                $data_arr['city_longi'] = $longi;
            }
        }
        return $data_arr;
    }

}

if (!function_exists('who_is_login')) {

    function who_is_login() {
        $CI = & get_instance();
        $user_id = $CI->session->userdata('user_id');
        $partner_id = $CI->session->userdata('partner_id');
        if ($user_id === null && $partner_id === null) {
            return false;
        } else if ($user_id !== null) {
            return 'user';
        } else {
            return 'partner';
        }
    }

}

if (!function_exists('is_message_member')) {

    function is_message_member($hash) {
        $ci = & get_instance();
        $user_id = get_session_user_id();
        $ci->load->model('Generic_model');
        $return = array('is_member' => false, 'is_owner' => false);
        $check = $ci->Generic_model->is_message_member($hash, $user_id);
        if (!$check) {
            return true;
        }
        return false;
    }

}

if (!function_exists('load_message')) {

    function load_message($hash) {
        $ci = & get_instance();
        $user_id = get_session_user_id();
        $ci->load->model('Generic_model');
        $check = $ci->Generic_model->load_message($hash, $user_id);
        return $check;
    }

}

if (!function_exists('build_message_html')) {

    function build_message_html($message_data) {
        $html = '';
        foreach ($message_data as $message) {
            if ($message['sender_id'] !== get_session_user_id()) {
                $html .= '<div class="row">
                  	<div class="col-md-10 pull-left">
                      <div class="chat-row well well-sm">
                        <div class="chat-avatar pull-left"><span class="chat-img">
                            <img src="' . base_url("uploads" . DIR_SEP . "profile-images" .
                                DIR_SEP . $message['sender_id'] . DIR_SEP . "thumb" . DIR_SEP . $message['photo']) . '" alt="' . $message['fname'] . '" title="' . $message['fname'] . '" class="img-circle" width="100">
                          </span></div>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">' . $message['fname'] . ' ' . $message['lname'] . '</strong>
                                </div>
                                <p>
                                  ' . $message['message_text'] . '
                                </p>
                            </div>
                        </div>
                    </div>
                  </div>';
            } else {
                $html .= '<div class="row">
                  	<div class="col-md-10 pull-right">
                      <div class="chat-row well well-sm">
                        <div class="chat-avatar pull-right"><span class="chat-img">
                            <img src="' . base_url("uploads" . DIR_SEP . "profile-images" .
                                DIR_SEP . $message['sender_id'] . DIR_SEP . "thumb" . DIR_SEP . $message['photo']) . '" alt="User Avatar" title="User Avatar" class="img-circle" width="100">
                          </span></div>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">You</strong>
                                </div>
                                <p>
                                  ' . $message['message_text'] . '
                                </p>
                            </div>
                        </div>
                    </div>
                  </div>';
            }
        }
        return $html;
    }

}

if (!function_exists('send_message')) {

    function send_message($hash, $text) {
        $ci = & get_instance();
        $user_id = get_session_user_id();
        $ci->load->model('Generic_model');
        $return = array('is_member' => false, 'is_owner' => false);
        $check = $ci->Generic_model->send_message($hash, $text);
        if (!$check) {
            return true;
        }
        return false;
    }

}

if (!function_exists('is_login')) {

    function is_login(&$CI) {
        $user_id = $CI->session->userdata('user_id');
        if ($user_id == '') {
            set_error_message(SESSION_EXPIRED);
            redirect(base_url());
        } else {
            return true;
        }
    }

}

if (!function_exists('is_login_partner')) {

    function is_login_partner(&$CI) {
        $user_id = $CI->session->userdata('partner_id');
        if ($user_id == '') {
            set_error_message(SESSION_EXPIRED);
            redirect(base_url());
        } else {
            return true;
        }
    }

}

if (!function_exists('get_photo_name_from_url')) {

    function get_photo_name_from_url($url) {
        if ($url != '') {
            $tmp = explode(DIR_SEP, $url);
            return end($tmp);
        }
    }

}

if (!function_exists('time_elapsed_string')) {

    function time_elapsed_string($datetime, $full = true) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
                //'s' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . '' : 'just now';
    }

}

if (!function_exists('__echo')) {

    function __echo($arr, $str = '') {
        return isset($arr[$str]) ? $arr[$str] : '';
    }

}

if (!function_exists('__dmy')) {

    function __dmy($str) {
        return isset($str) ? date("d-M-Y", strtotime($str)) : '';
    }

}

if (!function_exists('__ymd')) {

    function __ymd($str) {
        return isset($str) ? date("Y-m-d", strtotime($str)) : '';
    }

}

if (!function_exists('__isselected')) {

    function __isselected($arr, $str, $val) {
        if ($arr[$str] == $val)
            return 'selected';
        else
            return;
    }

}

if (!function_exists('is_checked')) {

    function is_checked($val = 0) {
        return ($val == 1 ? "checked" : "");
    }

}

if (!function_exists('get_country_ajax')) {

    function get_country_ajax($country_title = NULL) {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->country_list($country_title);
        return json_encode($data);
    }

}

if (!function_exists('logout')) {

    function logout() {
        $CI = &get_instance();
        $CI->session->sess_destroy();
        header("Location:" . base_url('login'));
    }

}

if (!function_exists('echo_get_lang')) {

    function echo_get_lang($key) {
        $CI = &get_instance();
        return $CI->lang->line($key, FALSE);
    }

}

if (!function_exists('print_get_lang')) {

    function print_get_lang($key, $val = array()) {
        $CI = &get_instance();
        return vsprintf($CI->lang->line($key, FALSE), $val);
    }

}

if (!function_exists('get_hover_links')) {

    function get_hover_links($ent) {
        //debug_print($ent, true);
        return '<ul class="hover-icon-group">' .
                '<li>
            <a data-original-title="View Details" rel="tooltip" class="fa fa-eye  box-icon-normal round"
              href="' . base_url() . $ent['permalink'] . '"></a></li>'
                .
                '<li><a data-original-title="Add to Inquiry" onclick="update_cart(\'add_to_cart\', \'' . $ent['hashval'] . '\')"
               rel="tooltip" class="fa fa-shopping-cart  box-icon-normal round"  href="javascript:void(0)"></a></li>'
                .
                '<li><a data-original-title="Add to Compare" rel="tooltip" class="fa fa-exchange box-icon-normal round"
                 href="javascript:void(0)"></a></li>'
                .
                '<li><a data-original-title="Send Inquiry" rel="tooltip" class="fa fa-paper-plane-o  box-icon-normal round"
                  href="javascript:void(0)"></a></li>'
                .
                '</ul>';
    }

}

if (!function_exists('get_rating')) {

    function get_rating($rating_no, $show = 'star') {
        if ($rating_no == '') {
            $rating_no = 0;
        }
        $rating_no = number_format($rating_no, 1);
        $int = round($rating_no, 1);
        $rating = '';
        //$rating = '<ul data-original-title="Rating ' . $rating_no . ' of 5" rel="tooltip" class="icon-group booking-item-rating-stars icon-group text-tiny text-color">';
        $full = 'fa-star';
        $empty = 'fa-star-o';
        $half = 'fa-star-half-empty';
        $count = 0;
        if ($int <= 5) {
            for ($i = $int; $i >= 1; $i--) {
                $rating .= '<i class="fa ' . $full . '"></i>';
                $count++;
            }

            if (($count < 5) && $i != 0) {
                $count++;
                $rating .= '<i class="fa ' . $half . '"></i>';
            }
            for ($i = 0; $i < (5 - $count); $i++) {
                $rating .= '<i class="fa ' . $empty . '"></i>';
            }
        } else {
            $rating .= '<i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>';
        }
        if ($show == 'star') {
            return $rating;
        } else if ($show == 'number') {
            return ' (<span class="booking-item-rating-number"><b>' . $rating_no . '</b> / 5</span>)';
        } else {
            return $rating . ' (<span class="booking-item-rating-number"><b>' . $rating_no . '</b> / 5</span>)';
        }
    }

}/*

  if (!function_exists('get_entity_by_hash')) {

  function get_entity_by_hash($hash) {
  if ($hash != '') {
  $ci = &get_instance();
  $ci->load->model('common/Entity_model');
  return $ci->Entity_model->get_entity_details($hash);
  }
  return '';
  }

  } */

if (!function_exists('get_entity_details_helper')) {

    function get_entity_details_helper($params) {
        $params['unit_id'] = $params['unique_id'];
        if (isset($params['unique_id'])) {
            $ci = &get_instance();
            $ci->load->model('common/Entity_model');
            return $ci->Entity_model->get_entity_details($params);
        }
        return '';
    }

}

if (!function_exists('get_entity_image')) {

    function get_entity_image($unit_id) {
        if ($unit_id != '') {
            $ci = &get_instance();
            $ci->load->model('common/Entity_model');
            $data = $ci->Entity_model->get_entity_thumb($unit_id);
            if ($data['path'] != '') {
                $src = base_url() . UPLOAD_DIR . ENTITY_IMAGE_DIR . MEDIUM_DIR . $data['path'];
            } else {
                $src = base_url() . UPLOAD_DIR . DEFAULT_DIR . ENTITY_IMAGE_DIR . MEDIUM_DIR . 'default-entity.png';
            }
        }
        return $src;
    }

}

if (!function_exists('get_entity_with_thumb_by_hash')) {

    function get_entity_with_thumb_by_hash($hash) {
        if ($hash != '') {
            $ci = &get_instance();
            $ci->load->model('common/Entity_model');
            $data = array();
            $entity_data = $ci->Entity_model->get_entity_details($hash);
            $data['entity_data'] = $entity_data[0];
            $data['entity_s_images'] = $ci->Entity_model->get_entity_images($data['entity_data']['entity_id'], 'THUMB');
            return $data;
        }
        return '';
    }

}

if (!function_exists('get_entity_thumb_header')) {

    function get_entity_thumb_header($product = array()) {
        if (array_key_exists('images', $product)) {
            if (array_key_exists(0, $product['images'])) {
                $src = UPLOAD_DIR . ENTITY_IMAGE_DIR . THUMB_DIR . $product['entity_id'] . '/' . $product['images'][0]['path'];
            } else {
                $src = base_url() . UPLOAD_DIR . DEFAULT_DIR . ENTITY_IMAGE_DIR . MEDIUM_DIR . 'default-entity.png';
            }
        } else {
            $src = base_url() . UPLOAD_DIR . DEFAULT_DIR . ENTITY_IMAGE_DIR . MEDIUM_DIR . 'default-entity.png';
        }
        $return = '<a target="_blank" href="' . base_url() . $product['permalink'] . '">
                            <img title="' . array_key_exists_else($product, 'entity_disp', "") . '"
                                alt="' . array_key_exists_else($product, 'entity_disp', "") . '"
                                title="' . array_key_exists_else($product, 'entity_disp', "") . '"
                                src="' . $src . '"
                                class="img-responsive">
                         </a>';
        return $return;
    }

}


if (!function_exists('array_column_multidimensional')) {

    function array_column_multidimensional($array, $key1) {
        $return = array();
        foreach ($array as $key => $value) {
            if (!array_key_exists($value[$key1], $return)) {
                $return[$value[$key1]] = array();
            }
            array_push($return[$value[$key1]], $value);
        }
        return $return;
    }

}

if (!function_exists('get_entity_thumb')) {

    function get_entity_thumb($product = array()) {
        $ameneties = '';
        if (count($product['ameneties'] > 0)) {
            $ameneties = 'Amenities<ul class="facilities-list clearfix">';
            foreach ($product['ameneties'] as $amenety) {
                $ame = $amenety['amenity'];
                $ameneties .= '<li class="small"><i title="' . $ame . '" class="' . $amenety['symbol'] . '"></i></li>';
            }
            $ameneties .= '</ul>';
        }
        switch (true) {
            case $product['price_flag'] == 0:
                $price_flag = 'Starting from';
                break;
            case $product['price_flag'] == 1:
                $price_flag = 'Instant Booking';
                break;
            default:
                $price_flag = '';
                break;
        }
        $return = '<div class="thumbnail recent-properties-box">
                    ' . get_entity_thumb_header($product) . '
                    <div class="caption detail">
                        <header class="clearfix">
                            <div class="pull-left"><h1 class="title"><a target="_blank"
                             href="' . base_url() . $product['city_name'] . '-' . $product['state_iso'] . '-' . $product['country_code'] . '/' . $product['permalink'] . '">'
                . array_key_exists_else($product, "entity_disp", "") . '</a></h1></div>
                        </header>
                        <h3 class="location"><a href="javascript:void(0)"><i class="fa fa-map-marker"></i>'
                . $product['city_name'] . ', ' . $product['state_name'] . ', ' . $product['country_name'] . '</a></h3>
                        ' . $ameneties . '
                        <div class="footer">
                            <div class="price green">' . $price_flag . ' ' . $product['currency_code'] . ' ' . $product['price'] . ' ' . $product['price_type'] . '</div>
                        </div>
                    </div>
                </div>';
        return $return;
    }

}

if (!function_exists('get_cart_entity_summary')) {

    function get_cart_entity_summary($entity = array()) {
        return '<div class="dd-product-group" id="pr5">
                    <div class="dd-product-box pull-left">
                        <a href="javascript:void(0)" title="product name">
                            <img src="' . base_url('uploads/defaults/entity_images/thumb/default-entity.png') . '" alt="Venue name" title="Venue name">
                        </a>
                    </div>
                    <div class="dd-product-desc pull-left">
                        <a class="title">Beautiful Fit Velvet Sweater For Him test for longer header test for longer header</a>
                        <a href="javascript:void(0)" class="close-btn ddr"  onclick="update_cart(\'remote_cart\', 5)"><i class="fa-times fa"></i></a>
                    </div>
                </div>';
    }

}

if (!function_exists('create_dir_path_if_not_exist')) {

    function create_dir_path_if_not_exist($dir, $mode = '0777') {
        if (!file_exists($dir)) {
            @mkdir($dir, 0755, true);
            return @chmod($dir, 0755);
        }
    }

}

if (!function_exists('reduce_size')) {

    function reduce_size($dir) {
        //$dir = '../uploads/';
        $pathToImages = opendir($dir);
        while (false !== ($fname = readdir($pathToImages))) {
            $file = $dir . $fname;
            echo $dir . $fname . '<br />';
            $im = @imagecreatefromjpeg($file); // original image
            @imagejpeg($im, $file, 90);
        }
    }

//reduce_size('../theme/site/idream/images/');
}

if (!function_exists('make_thumb')) {

    function make_thumb($src, $dest, $desired_width, $auto_height = false) {
        /* $source_image = imagecreatefromjpeg($src);
          $width = imagesx($source_image);
          $height = imagesy($source_image);
          if ($auto_height === true) {
          $desired_height = floor($height * ($desired_width / $width));
          } else if ($auto_height === false) {
          $desired_height = $desired_width;
          } else {
          $desired_height = $auto_height;
          }
          $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
          imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
          imagejpeg($virtual_image, $dest); */
        //echo $src.'---'. $dest.'---'. $desired_width.'---'. $auto_height;die;
        @chmod($src, 0755);
        $thumbnail_width = $desired_width;
        $thumbnail_height = $auto_height;
        $arr_image_details = getimagesize($src); // pass id to thumb name
        $original_width = $arr_image_details[0];
        $original_height = $arr_image_details[1];
        if ($original_width > $original_height) {
            $new_width = $thumbnail_width;
            $new_height = intval($original_height * $new_width / $original_width);
        } else {
            $new_height = $thumbnail_height;
            $new_width = intval($original_width * $new_height / $original_height);
        }
        $dest_x = intval(($thumbnail_width - $new_width) / 2);
        $dest_y = intval(($thumbnail_height - $new_height) / 2);
        if ($arr_image_details[2] == IMAGETYPE_GIF) {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
        }
        if ($arr_image_details[2] == IMAGETYPE_JPEG) {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
        }
        if ($arr_image_details[2] == IMAGETYPE_PNG) {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
        }
        if ($imgt) {
            $old_image = $imgcreatefrom($src);
            $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
            imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
            $imgt($new_image, $dest);
        }
    }

//make_thumb('../../theme/site/idream/images/', '../theme/site/idream/images/thumb/', 200);
}

if (!function_exists('convert_arr_to_string')) {

    function convert_arr_to_string($arr, $key) {
        $v = '';
        foreach ($arr as $val) {
            $v .= $val[$key] . ',';
        }
        $v = rtrim($v, ',');
        return $v;
    }

}

// Get only Entity related Amenities
if (!function_exists('get_amenities')) {

    function get_amenities($params = NULL) {
        $CI = &get_instance();
        $CI->load->model('common/Entity_model');
        $data = $CI->Entity_model->get_amenities($params);
        return $data;
    }

}

// Get all amenities from Master
if (!function_exists('get_all_amenities')) {

    function get_all_amenities() {
        $CI = &get_instance();
        $CI->load->model('common/Entity_model');
        $data = $CI->Entity_model->get_all_filter_amenities();
        return $data;
    }

}

// Get all Property Types from property_type_master
if (!function_exists('get_all_property_types')) {

    function get_all_property_types() {
        $CI = &get_instance();
        $CI->load->model('common/Entity_model');
        $data = $CI->Entity_model->get_all_property_types();
        return $data;
    }

}

// Get all services from service_master
if (!function_exists('get_all_services')) {

    function get_all_services() {
        $CI = &get_instance();
        $CI->load->model('common/Entity_model');
        $data = $CI->Entity_model->get_all_services();
        return $data;
    }

}

// Get entity services from service_master
if (!function_exists('get_entity_service')) {

    function get_entity_service($entity_hashval) {
        $CI = &get_instance();
        $CI->load->model('common/Entity_model');
        $data = $CI->Entity_model->get_entity_service($entity_hashval);
        return $data;
    }

}

// Get all taxes from tax_master
if (!function_exists('get_all_taxes')) {

    function get_all_taxes($city_id) {
        $CI = &get_instance();
        $CI->load->model('common/Entity_model');
        $data = $CI->Entity_model->get_all_taxes($city_id);
        return $data;
    }

}

// Get all policy categories from policy_category_master
if (!function_exists('get_policy_categories')) {

    function get_policy_categories() {
        $CI = &get_instance();
        $CI->load->model('common/Entity_model');
        $data = $CI->Entity_model->get_policy_categories();
        return $data;
    }

}

if (!function_exists('array_key_exists_else')) {

    function array_key_exists_else($array, $key, $value) {
        return (array_key_exists($key, $array) === true) ? $array[$key] : $value;
    }

}

if (!function_exists('debug_print')) {

    function debug_print($str, $exit = false) {
        echo '<pre>';
        print_r($str);
        echo '</pre>';
        if ($exit) {
            exit();
        }
    }

}

if (!function_exists('get_display_price')) {

    function get_display_price($disp_currency, $source_currency, $amount) {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_display_rate($disp_currency, $source_currency, $amount);
        if (count($data) > 1) {
            return $data['disp_rate'];
        } else {
            return 0;
        }
    }

}

if (!function_exists('get_calc_price')) {

    function get_calc_price($source_currency, $amount) {
        $calc_currency = 'CAD';
        return get_display_price($calc_currency, $source_currency, $amount);
    }

}

if (!function_exists('alternate_chunck')) {

    function alternate_chunck($array, $chunk_count) {
        $return = array();
        foreach ($array as $key => $value) {
            if ($key + 1 <= $chunk_count) {
                $return[$key] = array();
                array_push($return[$key], $value);
            } else {
                $key = (($key + 1) % $chunk_count) - 1;
                array_push($return[$key], $value);
            }
        }
        return $return;
    }

}

function add_to_cart_process($item_hash) {
    
}

function session_cart_add(&$ci, $current_sess_cart, $new_entity_id_or_hash, $id_or_hash, $entity_data = array()) {
    if (count($entity_data) == 0) {
        $entity_data = get_entity_with_thumb_by_hash($new_entity_id_or_hash);
    }

    $set = array('entity_id' => $entity_data['entity_data']['entity_id'],
        'name' => $entity_data['entity_data']['entity_disp'],
        'img' => $entity_data['entity_s_images'][0]['path'],
        'permalink' =>
        $entity_data['entity_data']['permalink'],
        'entity_hash' => $entity_data['entity_data']['hashval'],
        'city_name' => $entity_data['entity_data']['city_name'],
        'state_iso' => $entity_data['entity_data']['state_iso'],
        'country_code' => $entity_data['entity_data']['country_code']);
    if ($current_sess_cart == '' or $current_sess_cart == null) {
        $ci->session->set_userdata('cart', array($set));
    } else {
        $remove_key = array_search($new_entity_id_or_hash, array_column($current_sess_cart, $id_or_hash));
        if (($remove_key === false)) {
            array_push($current_sess_cart, $set);
            $ci->session->cart = $current_sess_cart;
        }
    }
}

if (!function_exists('currentUrl')) {

    function currentUrl() {
        $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']), 'https') === FALSE ? 'http' : 'https';
        $host = $_SERVER['HTTP_HOST'];
        $script = $_SERVER['SCRIPT_NAME'];
        $params = $_SERVER['QUERY_STRING'];
        return $protocol . '://' . $host . $script . '?' . $params;
    }

}

if (!function_exists('fulltext_split_words')) {

    function fulltext_split_words($search, $useanywords = 0) {
        $ary = explode(" ", $search);
        $mak = count($ary) - 1;
        $str = "";
        $i = 0;
        while ($i <= $mak) {
            if (trim($ary[$i] == '')) {
                $i++;
                continue;
            }
            if ($useanywords == 1) {
                $str .= '  ' . $ary[$i] . '* ';  /// +"mang"
            } else {
                $str .= ' +' . $ary[$i] . ' ';  /// +"mang"
            }
            $i++;
        }
        return $str;
    }

}

if (!function_exists('get_city_id')) {

    function get_city_id($city, $province, $country) {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $city_id = $CI->Generic_model->get_city_id($city, $province, $country);
        return $city_id;
    }

}

if (!function_exists('get_city_name')) {

    function get_city_name($city_id) {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $city_data = $CI->Generic_model->get_city_name($city_id);
        return $city_data;
    }

}

if (!function_exists('get_state_name')) {

    function get_state_name($state_id) {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $city_data = $CI->Generic_model->get_state_name($state_id);
        return $city_data;
    }

}

if (!function_exists('get_country_name')) {

    function get_country_name($country_id) {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $city_data = $CI->Generic_model->get_country_name($country_id);
        return $city_data;
    }

}

if (!function_exists('get_amenities_option')) {

    function get_amenities_option($selected) {
        $str = '';
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->amenities_list($selected);
        foreach ($data as $res) {
            $sel = '';
            if ($res['id'] == $selected)
                $sel = 'selected=selected';
            $str .= '<option value="' . $res['id'] . '" ' . $sel . '>' . $res['amenity'] . '</option>';
        }
        return $str;
    }

}

if (!function_exists('get_sales_rep')) {

    function get_sales_rep($selected) {
        $str = '';
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->sales_rep_list($selected);
        foreach ($data as $res) {
            $sel = '';
            if ($res['id'] == $selected)
                $sel = 'selected=selected';
            $str .= '<option value="' . $res['id'] . '" ' . $sel . '>' . $res['sales_rep'] . '</option>';
        }
        return $str;
    }

}

if (!function_exists('get_banquet_count')) {

    function get_banquet_count() {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_banquet_count();
        return $data['banquet_count'];
    }

}

if (!function_exists('get_user_count')) {

    function get_user_count() {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_user_count();
        return $data['user_count'];
    }

}

if (!function_exists('get_user_count')) {

    function get_user_count() {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_user_count();
        return $data['user_count'];
    }

}

if (!function_exists('user_chat_count')) {

    function user_chat_count($id) {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_user_chat_count($id);
        return $data['em_count'];
    }

}

if (!function_exists('get_booking_count')) {

    function get_booking_count() {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_booking_count();
        return $data['booking_count'];
    }

}

if (!function_exists('get_recent_venue_providers')) {

    function get_recent_venue_providers() {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_recent_venue_providers();
        return $data;
    }

}

if (!function_exists('get_recent_vendors')) {

    function get_recent_vendors() {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_recent_vendors();
        return $data;
    }

}

if (!function_exists('get_recent_users')) {

    function get_recent_users() {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_recent_users();
        return $data;
    }

}

if (!function_exists('get_my_recent_customer_requests')) {

    function get_my_recent_customer_requests($user_id) {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_my_recent_customer_requests($user_id);
        return $data;
    }

}

if (!function_exists('get_my_recent_confirmed_bookings')) {

    function get_my_recent_confirmed_bookings($user_id) {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_my_recent_confirmed_bookings($user_id);
        return $data;
    }

}

if (!function_exists('get_my_recent_payments')) {

    function get_my_recent_payments($user_id) {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $data = $CI->Generic_model->get_my_recent_payments($user_id);
        return $data;
    }

}

if (!function_exists('build_sql_in_clause_from_csv')) {

    function build_sql_in_clause_from_csv($csv) {
        return "IN ('" . str_replace(",", "','", $csv) . "') ";
    }

}

if (!function_exists('build_search_form_field')) {

    function build_search_form_field() {
        $CI = &get_instance();
        $CI->load->model('common/Generic_model');
        $search_form = array();
        $search_form['event_type'] = $CI->Generic_model->get_event_type_for_search_form();
        $search_form['guests'] = $CI->Generic_model->get_guests_for_search_form();
        $search_form['selected']['event_type'] = '';
        $search_form['selected']['guests'] = '';
        $search_form['selected']['search_date'] = '';
        $search_form['selected']['starttime'] = '';
        $search_form['selected']['duration'] = '';
        $search_form['selected']['duration_type'] = '';
        $search_form['selected']['location'] = '';
        $search_form['selected']['zipcode_loc'] = '';
        $search_form['selected']['search_key'] = '';
        $search_form['selected']['place_type'] = '';
        $search_form['selected']['lat'] = '';
        $search_form['selected']['lng'] = '';
        return $search_form;
    }

}


if (!function_exists('get_seo_friendly_url')) {

    function get_seo_friendly_url($str) {
        $str = strtolower(str_replace(" ", "-", $str));
        $str = strtolower(str_replace("&", "and", $str));
        $str = strtolower(str_replace("/", "-", $str));
        $str = strtolower(str_replace("\\", "-", $str));
        $str = strtolower(str_replace(".", "-", $str));
        $str = strtolower(str_replace(",", "-", $str));
        $str = strtolower(str_replace("--", "-", $str));
        return $str;
    }

}


if (!function_exists('get_entity_permalink')) {

    function get_entity_permalink($name) {
        $permalink = get_seo_friendly_url($name);
        $permalink = str_replace("--", "-", $permalink);
        $permalink = ltrim($permalink, '-');
        $permalink = rtrim($permalink, '-');
        return $permalink;
    }

}


if (!function_exists('get_image_display_path_by_name')) {

    function get_image_display_path_by_name($type, $id, $image_size_str, $image_name) {
        $image_size = LARGE_DIR;
        switch (strtolower($image_size_str)) {
            case 'thumb':
                $image_size = THUMB_DIR;
                break;
            case 'medium':
                $image_size = MEDIUM_DIR;
                break;
        }
        switch (strtolower($type)) {
            case 'entity':
                return base_url() . UPLOAD_DIR . ENTITY_IMAGE_DIR . $image_size . $id . DIR_SEP .
                        $image_name;
                break;
        }
    }

}


if (!function_exists('rand_uniqid')) {

    function rand_uniqid($in, $to_num = false, $pad_up = false, $passKey = null) {
        $index = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if ($passKey !== null) {
            // Although this function's purpose is to just make the
            // ID short - and not so much secure,
            // you can optionally supply a password to make it harder
            // to calculate the corresponding numeric ID

            for ($n = 0; $n < strlen($index); $n++) {
                $i[] = substr($index, $n, 1);
            }

            $passhash = hash('sha256', $passKey);
            $passhash = (strlen($passhash) < strlen($index)) ? hash('sha512', $passKey) : $passhash;

            for ($n = 0; $n < strlen($index); $n++) {
                $p[] = substr($passhash, $n, 1);
            }

            array_multisort($p, SORT_DESC, $i);
            $index = implode($i);
        }

        $base = strlen($index);

        if ($to_num) {
            // Digital number  <<--  alphabet letter code
            $in = strrev($in);
            $out = 0;
            $len = strlen($in) - 1;
            for ($t = 0; $t <= $len; $t++) {
                $bcpow = bcpow($base, $len - $t);
                $out = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
            }

            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $out -= pow($base, $pad_up);
                }
            }
            $out = sprintf('%F', $out);
            $out = substr($out, 0, strpos($out, '.'));
        } else {
            // Digital number  -->>  alphabet letter code
            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $in += pow($base, $pad_up);
                }
            }

            $out = "";
            for ($t = floor(log($in, $base)); $t >= 0; $t--) {
                $bcp = bcpow($base, $t);
                $a = floor($in / $bcp) % $base;
                $out = $out . substr($index, $a, 1);
                $in = $in - ($a * $bcp);
            }
            $out = strrev($out); // reverse
        }

        return $out;
    }

    if (!function_exists('get_compare_count')) {

        function get_compare_count($session_id) {
            $CI = &get_instance();
            $CI->load->model('common/Entity_model');
            $data = $CI->Entity_model->get_compare_list($session_id);
            return count($data);
        }

    }

    if (!function_exists('get_sent_requests_count_cust')) {

        function get_sent_requests_count_cust($customer_id) {
            $CI = &get_instance();
            $CI->load->model('common/Inquiry_model');
            $count = $CI->Inquiry_model->get_sent_requests_count_cust($customer_id);
            return $count;
        }

    }

    if (!function_exists('get_quote_received_count_cust')) {

        function get_quote_received_count_cust($customer_id) {
            $CI = &get_instance();
            $CI->load->model('common/Inquiry_model');
            $count = $CI->Inquiry_model->get_quote_received_count_cust($customer_id);
            return $count;
        }

    }

    if (!function_exists('get_booking_count_cust')) {

        function get_booking_count_cust($customer_id) {
            $CI = &get_instance();
            $CI->load->model('common/Inquiry_model');
            $count = $CI->Inquiry_model->get_booking_count_cust($customer_id);
            return $count;
        }

    }

    if (!function_exists('get_sent_requests_count_partner')) {

        function get_sent_requests_count_partner($entity_id) {
            $CI = &get_instance();
            $CI->load->model('common/Inquiry_model');
            $count = $CI->Inquiry_model->get_sent_requests_count_partner($entity_id);
            return $count;
        }

    }

    if (!function_exists('get_status_text')) {

        function get_status_text($str) {
            $CI = &get_instance();
            $CI->load->model('common/Inquiry_model');
            $count = $CI->Inquiry_model->get_status_text($str);
            return $count;
        }

    }

    if (!function_exists('get_seo_data')) {

        function get_seo_data($uri_segment) {
            $CI = &get_instance();
            $CI->load->model('common/Entity_model');
            $seo_data = $CI->Entity_model->get_seo_data($uri_segment);
            return $seo_data;
        }

    }

    if (!function_exists('get_sla_draft')) {

        function get_sla_draft() {
            $CI = &get_instance();
            $CI->load->model('frontend/Pages_model');
            $sla = $CI->Pages_model->get_master_sla();
            return $sla;
        }

    }

    if (!function_exists('get_user_sla_details')) {

        function get_user_sla_details($user_hashval) {
            $CI = &get_instance();
            $CI->load->model('frontend/Pages_model');
            $sla = $CI->Pages_model->get_user_sla_details($user_hashval);
            return $sla;
        }

    }

    if (!function_exists('get_user_details')) {

        function get_user_details($params) {
            $CI = &get_instance();
            $CI->load->model('realnet/User_model');
            $sla = $CI->User_model->user_details($params);
            return $sla;
        }

    }


    if (!function_exists('get_user_sent_req_count')) {

        function get_user_sent_req_count() {
            $CI = &get_instance();
            $CI->load->model('common/Inquiry_model');
            $sent_req_data = $CI->Inquiry_model->get_customer_requests();
            $sent_req_count = count($sent_req_data);
            return $sent_req_count;
        }

    }
    if (!function_exists('get_user_confirm_booking_count')) {

        function get_user_confirm_booking_count() {
            $CI = &get_instance();
            $CI->load->model('common/Inquiry_model');
            $sent_confirm_booking_data = $CI->Inquiry_model->get_confirmed_bookings_cust();
            $sent_confirm_booking_count = count($sent_confirm_booking_data);
            return $sent_confirm_booking_count;
        }

    }
    if (!function_exists('get_user_quotes_received_count')) {

        function get_user_quotes_received_count() {
            $CI = &get_instance();
            $CI->load->model('common/Inquiry_model');
            $qoute_received_data = $CI->Inquiry_model->get_quote_received_cust();
            $qoute_received_data_count = count($qoute_received_data);
            return $qoute_received_data_count;
        }

    }
    if (!function_exists('get_user_booking_history')) {

        function get_user_booking_history() {
            $CI = &get_instance();
            $CI->load->model('common/Inquiry_model');
            $booking_history_data = $CI->Inquiry_model->get_confirmed_bookings_cust_history();
            $booking_history_data_count = count($booking_history_data);
            return $booking_history_data_count;
        }

    }

    if (!function_exists('set_user_search_session')) {

        function set_user_search_session(&$ci, $data) {
            $ci->session->unset_userdata('search_string');
            $s_string = array(
                'search_key' => $data['search_key'],
                'lat' => $data['lat'],
                'long' => $data['long'],
                'type' => $data['type'],
                'bedrooms' => $data['bedrooms'],
                'washrooms' => $data['washrooms'],
                'place_sub_type' => $data['place_sub_type'],
                'pets' => $data['pets'],
                'price' => $data['price']
            );
            $ci->session->set_userdata('search_string', $s_string);
        }

    }

    if (!function_exists('get_promo_code')) {

        function get_promo_code() {
            $CI = &get_instance();
            $CI->load->model('common/Generic_model');
            $search_form = array();
            $data = $CI->Generic_model->get_promo_code();
            return $data;
        }

    }

    if (!function_exists('generateRandomString')) {

        function generateRandomString($length = 5) {
            return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
        }

    }

    if (!function_exists('get_building_id_count')) {

        function get_building_id_count($building_id) {
            $ci = &get_instance();
            $ci->load->model('common/Entity_model');
            return $ci->Entity_model->count_building_id($building_id);
        }

    }

    if (!function_exists('set_pagination')) {

        function set_pagination($base_url, $total_rows, $per_page, $uri_segment) {
            $ci = &get_instance();
            $ci->load->library('pagination');
            $ci->load->library('pagination');
            $config = array();
            $config["base_url"] = $base_url;
            $config["total_rows"] = $total_rows;
            $config["per_page"] = $per_page;
            $config["uri_segment"] = $uri_segment;

            //Pagination Style
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';

            $config['next_link'] = 'Next Page';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';

            $config['prev_link'] = 'Previous Page';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
            $ci->pagination->initialize($config);

            $page = ($ci->uri->segment($uri_segment)) ? $ci->uri->segment($uri_segment) : 0;
            $data['page']= $page;
            $data['links'] = $ci->pagination->create_links();
            return $data;
            
        }

    }
}