<?php

    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class google extends CI_Model {

        private $apikey = GOOGLE_MAPS_API_EMBED;

        function __construct() {
            parent::__construct();
            $this->load->database();
        }

        function get_lat_long($address) {
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . "&key=" . $this->apikey;
            $result = $this->curl_get($url);
            $x = json_decode($result);
            return $x->results[0];
        }

        function curl_get($myurl) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $myurl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
        }

    }

?>