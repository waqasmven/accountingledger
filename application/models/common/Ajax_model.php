<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajax_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function search_entity($keyword, $type) {
        $query = '';
        $like_param = "%" . $keyword . "%";
        $entity_query = "SELECT * FROM `entity_master_list` WHERE (`entity_name` LIKE ? OR entity_disp LIKE ? OR address LIKE ?) GROUP BY entity_id LIMIT 3 ";
        $add_type = false;
        switch ($type) {
            case "hotel":
                $query .= " AND `entity_type` = ?  ";
                $add_type = true;
                break;
            case "all":
            default :
        }

        if ($add_type) {
            $entity_stmt = $this->db->conn_id->prepare($entity_query);
            $entity_stmt->execute(array($like_param, $like_param, $like_param, $type));
        } else {
            $entity_stmt = $this->db->conn_id->prepare($entity_query);
            $entity_stmt->execute(array($like_param, $like_param, $like_param));
        }

        $entity_data = $entity_stmt->fetchAll(PDO::FETCH_ASSOC);
        //debug_print($entity_data);
        $result_array = array();
        foreach ($entity_data as $e_d) {
            array_push($result_array, array(
                    'label' => 'city',
                    'value' => $e_d['entity_disp'],
                    'address' => $e_d['address'],
                    'lat' => $e_d['lati'],
                    'long' => $e_d['longi']
                )
            );
        }
        return $result_array;
    }
}

?>