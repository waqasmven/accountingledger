<?php

class Inquiry_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function get_customer_event_plan_inquiry() {

        $conn = $this->db->conn_id;

        $query = "SELECT * FROM `customer_plan_request_list` WHERE customer_id = " . get_session_user_id() . " ORDER BY
        customer_event_plan_id DESC";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $return = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

    function get_customer_requests() {
        $conn = $this->db->conn_id;
        $query = "SELECT * FROM `customer_requests_list` WHERE request_process = 'REQ_SENT_CUST' AND created_by = " . get_session_user_id() . " ORDER BY modified_date DESC";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $return = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

    function get_quote_received_cust() {
        $conn = $this->db->conn_id;
        $query = "SELECT * FROM `customer_requests_list` WHERE request_process = 'QUOTE_SENT_PARTNER' AND created_by = " . get_session_user_id() . " ORDER BY modified_date DESC";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $return = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

    function get_accepted_quote_cust() {
        $conn = $this->db->conn_id;
        $query = "SELECT * FROM `customer_requests_list` WHERE request_process = 'QUOTE_ACCEPT_CUST' AND created_by = " . get_session_user_id() . " ORDER BY modified_date DESC";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $return = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

    function get_quote_sent_partner() {
        $user_id = get_session_user_id();
        $conn = $this->db->conn_id;
        $query = "SELECT * FROM `customer_requests_list` WHERE user_id = ? AND request_process = 'QUOTE_SENT_PARTNER' ORDER BY
        modified_date DESC";
        $stmt = $conn->prepare($query);
        $stmt->execute(array($user_id));
        $return = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

    function get_confirmed_bookings_cust() {
        $user_id = get_session_user_id();
        $conn = $this->db->conn_id;
        $date = date('Y-m-d h:i:s');
        $query = "SELECT * FROM `customer_requests_list` WHERE created_by = " . get_session_user_id() . " AND request_process = 'BOOKING_CONFIRM_CUST' AND from_date > ? ORDER BY
        modified_date DESC";
        $stmt = $conn->prepare($query);
        $stmt->execute(array($date));
        $return = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

    function get_confirmed_bookings_cust_history() {
        $user_id = get_session_user_id();
        $conn = $this->db->conn_id;
        $date = date('Y-m-d h:i:s');
        $query = "SELECT * FROM `customer_requests_list` WHERE created_by = " . get_session_user_id() . " AND request_process = 'BOOKING_CONFIRM_CUST' AND from_date < ? ORDER BY
        modified_date DESC";
        $stmt = $conn->prepare($query);
        $stmt->execute(array($date));
        $return = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

    function get_confirmed_bookings() {
        $user_id = get_session_user_id();
        $conn = $this->db->conn_id;
        $query = "SELECT * FROM `customer_requests_list` WHERE user_id = ? AND request_process = 'BOOKING_CONFIRM_CUST' ORDER BY
        modified_date DESC";
        $stmt = $conn->prepare($query);
        $stmt->execute(array($user_id));
        $return = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

    function get_customer_requests_details($request_hasval) {
        $conn = $this->db->conn_id;
        $query = "SELECT * FROM `service_request_list` WHERE req_hashval = ?";
        $stmt = $conn->prepare($query);
        $stmt->execute(array($request_hasval));
        $return = $stmt->fetch(PDO::FETCH_ASSOC);
        return $return;
    }

    function get_service_requests_details($request_hasval) {
        $conn = $this->db->conn_id;
        $query = "SELECT * FROM `customer_request_list` WHERE req_hashval = ?";
        $stmt = $conn->prepare($query);
        $stmt->execute(array($request_hasval));
        $return = $stmt->fetch(PDO::FETCH_ASSOC);
        return $return;
    }

    function get_customer_requests_items($request_hasval) {
        $conn = $this->db->conn_id;
        $query = "SELECT * FROM `customer_request_details_list` WHERE request_hashval = ? ORDER BY id";
        $stmt = $conn->prepare($query);
        $stmt->execute(array($request_hasval));
        $return = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

    function get_quote_detail_by_req_entity($customer_request_hashval, $entity_hash) {

        $conn = $this->db->conn_id;

        $query = "SELECT
                    cprl.customer_fname,
                    cprl.customer_lname,
                    cprl.requested_date,
                    cprl.type,
                    cprl.event_guest_count,
                    cprl.event_date,
                    cprl.event_start_time,
                    cprl.event_duration,
                    cprl.additional_servies,
                    cprl.entity_id,
                    cprl.entity_disp,
                    cprl.request_date,
                    ql.*
                    FROM customer_plan_request_list cprl
                    LEFT JOIN quote_list ql ON cprl.customer_request_id = ql.request_id
                    WHERE cprl.customer_request_hashval = :customer_request_hashval AND cprl.entity_hash = :entity_hash";
        $stmt = $conn->prepare($query);
        $stmt->execute(array(
            ':entity_hash' => $entity_hash,
            ':customer_request_hashval' => $customer_request_hashval
        ));
        $return = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

    function update_additional_services($additional_service, $customer_event_plan_hash, $conn) {

        if ($conn === null) {
            $conn = $this->db->conn_id;
        }
        $query = "UPDATE `customer_event_plan` SET `additional_servies` = :additional_servies
                      WHERE customer_event_hash = :customer_event_hash  ";
        $stmt = $conn->prepare($query);
        $stmt->execute(
                array(
                    ':additional_servies' => $additional_service,
                    ':customer_event_hash' => $customer_event_plan_hash
        ));
    }

    function update_request_process_status($params) {
        $conn = $this->db->conn_id;
        $request_hashval = $params['request_hashval'];
        $request_process = $params['request_process'];
        $modified_by = $params['modified_by'];
        $query = "UPDATE `customer_requests` SET `request_process` = ?, modified_by = ? WHERE request_hashval = ?";
        $stmt = $conn->prepare($query);
        $stmt->execute(array($request_process, $modified_by, $request_hashval));
        //echo var_export($stmt->errorInfo()); exit;
        $data['request_hashval'] = $request_hashval;
        return $data;
    }

    function addedit_quote($params) {
        $conn = $this->db->conn_id;
        $request_hashval = $params['request_hashval'];
        $servicesArr = $params['service_id'];
        $taxesArr = $params['tax_id'];
        $taxlabelArr = $params['tax_label'];
        $priceArr = $params['price'];
        $pricetypeArr = $params['price_type'];
        $qtyArr = $params['qty'];
        $selltotalArr = $params['sell_total'];
        $partner_id = $this->session->userdata('partner_id');
        $final_total = $params['final_total'];
        $request_process = 'QUOTE_SENT_PARTNER';
        $taxes = ''; // Calculate total taxes
        if ($request_hashval != '') {
            // Insert / Update services into request_details table
            for ($i = 0; $i < count($servicesArr); $i++) {
                $service_id = $servicesArr[$i];
                $service_type = 'SERVICE';
                $price = $priceArr[$i];
                $price_type = $pricetypeArr[$i];
                $qty = $qtyArr[$i];
                $sell_total = $selltotalArr[$i];
                $query = "INSERT INTO `customer_request_details` (request_hashval, item_id, item_type, qty, base_price, price_type, final_price) "
                        . "VALUES(?, ?, ?, ?, ?, ?, ?)";
                $stmt = $conn->prepare($query);
                $stmt->execute(array($request_hashval, $service_id, $service_type, $qty, $price, $price_type, $sell_total));
                //echo var_export($stmt->errorInfo()); exit;
            }

            // Insert / Update taxes into request_details table
            for ($i = 0; $i < count($taxesArr); $i++) {
                $tax_id = $taxesArr[$i];
                $tax_label = $taxlabelArr[$i];
                $service_type = 'TAX';
                $price = $params["$tax_label"];
                $price_type = '';
                $qty = '';
                $query = "INSERT INTO `customer_request_details` (request_hashval, item_id, item_type, qty, base_price, price_type, final_price) "
                        . "VALUES(?, ?, ?, ?, ?, ?, ?)";
                $stmt = $conn->prepare($query);
                $stmt->execute(array($request_hashval, $tax_id, $service_type, $qty, $price, $price_type, $price));
                //echo var_export($stmt->errorInfo()); exit;
                $taxes = floatval($taxes) + floatval($price);
            }

            // Update customer Request Table
            $query = "UPDATE `customer_requests` SET final_price=?, taxes=?, request_process=?, modified_by=?, modified_date = NOW() "
                    . "WHERE request_hashval=?";
            $stmt = $conn->prepare($query);
            $stmt->execute(array($final_total, $taxes, $request_process, $partner_id, $request_hashval));
            //echo var_export($stmt->errorInfo()); exit;
            $data['request_hashval'] = $request_hashval;
            return $data;
        }
    }

    function update_user_session_cart($cart_entites, $new_status, $old_status, $user_id, $conn) {

        if ($conn === null) {
            $conn = $this->db->conn_id;
        }
        $query = "UPDATE `user_session_cart` SET `status` = :new_status
                      WHERE `status` = :old_status AND entity_id IN (" . implode(',', $cart_entites) . ")
                       AND  user_id = :user_id ";
        $stmt = $conn->prepare($query);
        $stmt->execute(
                array(
                    ':new_status' => $new_status,
                    ':old_status' => $old_status,
                    ':user_id' => $user_id
        ));
    }

    function insert_customer_requests($cart_entity, $customer_request_hash, $message_hash, $customer_event_plan_hash, $conn) {
        if ($conn === null) {
            $conn = $this->db->conn_id;
        }
        $ref = rand_uniqid($cart_entity, false, 6, $customer_request_hash);
        $query = "INSERT INTO `customer_requests`(
                    `customer_request_hashval`,
                    `customer_event_plan_id`,
                    `entity_id`,
                    `ref_num`,
                    `message_hash`,
                    `customer_status`,
                    `partner_status`,
                    `partner_first_view`,
                    `partner_first_quote`,
                    `created_by`,
                    `created_date`,
                    `modified_by`,
                    `modified_date`) SELECT  :customer_request_hashval, id, :entity_id,
                        :ref, :message_hash, 'SENT', 'RECEIVED', null, null, " . get_session_user_id() . ",
                        current_timestamp," . get_session_user_id() . ", current_timestamp
                    FROM `customer_event_plan` where  customer_event_hash = :customer_event_hash";

        $stmt = $conn->prepare($query);
        $stmt->execute(
                array(
                    ':customer_request_hashval' => $customer_request_hash,
                    ':entity_id' => $cart_entity,
                    ':ref' => $ref,
                    ':customer_event_hash' => $customer_event_plan_hash,
                    ':message_hash' => $message_hash
        ));
    }

    function get_sent_requests_count_cust($customer_id) {
        $query = "SELECT count(id) as cust_req_count FROM customer_requests_list WHERE created_by = ? AND request_process = 'REQ_SENT_CUST'";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($customer_id));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['cust_req_count'];
    }

    function get_quote_received_count_cust($customer_id) {
        $query = "SELECT count(id) as quote_received_count FROM customer_requests_list WHERE created_by = ? AND request_process = 'QUOTE_SENT_PARTNER'";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($customer_id));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['quote_received_count'];
    }

    function get_booking_count_cust($customer_id) {
        $query = "SELECT count(id) as quote_received_count FROM customer_requests_list WHERE created_by = ? AND request_process = 'BOOKING_CONFIRM_CUST'";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($customer_id));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['quote_received_count'];
    }

    function get_sent_requests_count_partner($entity_id) {
        $query = "SELECT count(id) as partner_req_count FROM customer_requests_list WHERE entity_id = ? AND request_process = 'REQ_SENT_CUST'";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($entity_id));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['partner_req_count'];
    }

    function get_status_text($str) {
        $query = "SELECT * FROM status_desc WHERE status = ?";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($str));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['status_desc'];
    }

}
