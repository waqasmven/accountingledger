<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Generic_Model
 *
 * @author Sunil Khandade
 */
class Generic_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function email_log($params = array()) {
        $email_type = $params['email_type'];
        $email_to = $params['email_to'];
        $email_cc = $params['email_cc'];
        $email_subject = $params['email_subject'];
        $stmt = $this->db->conn_id->prepare("INSERT INTO email_log (email_type, email_to, email_cc, email_subject) VALUES (?, ?, ?, ?)");
        $stmt->execute(array($email_type, $email_to, $email_cc, $email_subject));
        //echo var_export($stmt->errorInfo());die;
        $last_id = $this->db->insert_id();
        $data['last_id'] = $last_id;
        return $data;
    }
    
    function send_message($sender, $receiver, $message_hash, $message_text) {
        $query = "INSERT INTO `messages` (`message_hash`, `sender_id`, `receiver_id`, `previous_message_id`, `message_text`) 
SELECT :message_hash, :sender, :receiver, id, :message_text FROM messages WHERE message_hash = :message_hash
ORDER BY ID DESC LIMIT 1";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array(
            ':message_hash' => $message_hash,
            ':sender' => $sender,
            ':receiver' => $receiver,
            ':message_text' => $message_text
        ));
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    function get_email_template($purpose, $emailType) {
        if ($emailType != '') {
            $email_template_query = "SELECT id, purpose, email_type, email_subject, CONCAT(email_header, email_body, email_footer) as email_body, status 
                                        FROM email_template WHERE purpose = ? AND email_type = ? AND STATUS = 'ACTIVE'";
            $email_template_stmt = $this->db->conn_id->prepare($email_template_query);
            $email_template_stmt->execute(array($purpose, $emailType));
        } else {
            return '';
        }
        $email_template_result = $email_template_stmt->fetch(PDO::FETCH_ASSOC);
        if (count($email_template_result) > 1) {
            return $email_template_result;
        }
        return '';
    }

    /**
     * This function will do insert into activity_log table.
     *
     * @author  Sunil Khandade
     *
     * @since 1.0
     *
     * @param array $params Array consisting of following fields.
     * user_id (int), module (varchar), activity (varchar), data (text)
     */
    function activity_log($params = array()) {

        $user_id = strictEmpty($params['modified_by_id']) ? 0 : $params['modified_by_id'];
        $module = $params['module'];
        $activity = $params['activity'];
        $data = isset($params['data']) ? $params['data'] : '';
        $ip_address = isset($params['ip_address']) ? $params['ip_address'] : getIP();
        $table = 'activity_log';

        switch (strtolower($module)) {
            case 'search':
                $table = 'search_log';
                break;
        }
        //debug_print($table, true);
        $activity_log_query = "INSERT INTO $table (created_date, user_id, module, activity,data,ip_address) 
                                        VALUES (CURRENT_TIMESTAMP , :user_id, :module, :activity,:data,:ip_address)";

        try {
            $stmt = $this->db->conn_id->prepare($activity_log_query);
            $stmt->execute(array(
                ':user_id' => $user_id,
                ':module' => $module,
                ':activity' => $activity,
                ':data' => $data,
                ':ip_address' => $ip_address
            ));
        } catch (Exception $e) {
            debug_print($e);
        }
        return;
    }
    
    function country_list($autocomplete = NULL) {
        $autocomplete = str_replace("0", "", $autocomplete);
        $country_query = "SELECT id, country_name FROM country_master WHERE 1=1 ";
        $country_query .= " AND id = 38"; // Remove this in production - By Sunil
        $orderby = " ORDER BY country_name";
        $stmt = $this->db->conn_id->prepare($country_query . $orderby);
        $stmt->execute();
        //echo var_export($stmt->errorInfo());
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function state_list($country_id) {
        $country_query = "SELECT id, state_name FROM state_master WHERE country_id = '$country_id' ";
        $orderby = " ORDER BY state_name";
        $stmt = $this->db->conn_id->prepare($country_query . $orderby);
        $stmt->execute();
        //echo var_export($stmt->errorInfo());
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_city_name($city_id) {
        $select = "SELECT id, city_name, state_id FROM city_master WHERE id = ? ";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($city_id));
        //echo var_export($stmt->errorInfo());
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_state_name($state_id) {
        $select = "SELECT id, state_name, country_id FROM state_master WHERE id = ? ";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($state_id));
        //echo var_export($stmt->errorInfo());
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_country_name($country_id) {
        $select = "SELECT * FROM country_master WHERE id = ? ";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute(array($country_id));
        //echo var_export($stmt->errorInfo());
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_city_id($city, $province, $country) {
        // Get Country id first
        $country_query = "SELECT id FROM country_master WHERE country_name = ?";
        $country_stmt = $this->db->conn_id->prepare($country_query);
        $country_stmt->execute(array($country));
        $country = $country_stmt->fetch(PDO::FETCH_ASSOC);
        $country_id = $country['id'];

        // Get State / Province id 
        $state_query = "SELECT id FROM state_master WHERE (state_name = ? OR state_iso = ?) AND country_id = ?";
        $state_stmt = $this->db->conn_id->prepare($state_query);
        $state_stmt->execute(array($province, $province, $country_id));
        $state = $state_stmt->fetch(PDO::FETCH_ASSOC);
        $state_id = $state['id'];

        $select_query = "SELECT id FROM city_master WHERE city_name = ? AND state_id = ?";
        $stmt = $this->db->conn_id->prepare($select_query);
        $stmt->execute(array($city, $state_id));
        $arr = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($arr['id'] == '') {
            $insert_query = "INSERT INTO city_master (city_name, state_id) VALUES (?, ?)";
            $insert_stmt = $this->db->conn_id->prepare($insert_query);
            $insert_stmt->execute(array($city, $state_id));
            $generated_id = $this->db->insert_id();
            return $generated_id;
        } else {
            return $arr['id']; // Return only city id
        }
    }

    function get_city_lat_long($city, $province, $country) {
        // Get Country id first
        $country_query = "SELECT city_id,city_lati, city_longi FROM city_province_country_list WHERE 
            city_name = '$city' AND state_iso = '$province' AND country_name = '$country'";
        $country_stmt = $this->db->conn_id->prepare($country_query);
        $country_stmt->execute();
        $country = $country_stmt->fetch(PDO::FETCH_ASSOC);
        return $country;

    }

    function get_master_sla($sla_id) {
        $select = "SELECT * FROM `sla_master` WHERE status = 'ACTIVE' AND id = '$sla_id'";
        $select_stmt = $this->db->conn_id->prepare($select);
        $select_stmt->execute();
        return $select_stmt->fetch(PDO::FETCH_ASSOC);
    }

    function update_city_lat_long($lati, $longi, $city_id) {
        // Get Country id first
        $country_query = "update city_master set
                                city_lati = $lati,
                                city_longi = $longi
                                where id = $city_id ";
        $country_stmt = $this->db->conn_id->prepare($country_query);
        $country_stmt->execute();
        return;

    }

    function update_partner_hashval($hashval, $user_id) {
        $user_hash_update_query = "UPDATE user_master SET hashval = ? WHERE id = ?";
        $user_hash_update_stmt = $this->db->conn_id->prepare($user_hash_update_query);
        $user_hash_update_stmt->execute(array($hashval, $user_id));
    }

    function get_title_meta($class = "default", $method = "default", $parameter = "default") {
        $select_seo_query = "UPDATE partner_master SET hashval = ? WHERE id = ?";
        $select_seo_stmt = $this->db->conn_id->prepare($select_seo_query);
        $select_seo_query->execute(array($hashval, $user_id));
    }

    function get_display_rate($disp_currency, $source_currency, $amount) {
        $select = "SELECT  (" . $amount . "/sr.torate)*dr.torate AS disp_rate FROM daily_rates dr,
                    (SELECT tocurrencycode, torate, basecurrency FROM daily_rates WHERE tocurrencycode = '" . $source_currency . "') sr
                    WHERE  dr.tocurrencycode = '" . $disp_currency . "'  AND dr.basecurrency = sr.basecurrency";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function get_capacity_measure() {
        $select = "SELECT id, capacity, orderby FROM guest_capacity ORDER BY orderby";
        $stmt = $this->db->conn_id->prepare($select);
        $stmt->execute();
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function amenities_list($autocomplete = NULL) {
        $amenity_query = "SELECT id, LOWER(amenity) as amenity FROM amenity_master WHERE status = 'ACTIVE' ";
        $orderby = " ORDER BY amenity";
        $stmt = $this->db->conn_id->prepare($amenity_query . $orderby);
        $stmt->execute();
        //echo var_export($stmt->errorInfo());
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function sales_rep_list($autocomplete = NULL) {
        $sales_rep_query = "SELECT id, CONCAT(fname, ' ', lname) as sales_rep FROM user_master_list WHERE status = 'ACTIVE' AND (user_type = 'SR' OR user_type = 'ADMIN') ";
        $orderby = " ORDER BY fname";
        $stmt = $this->db->conn_id->prepare($sales_rep_query . $orderby);
        $stmt->execute();
        //echo var_export($stmt->errorInfo());
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_banquet_count() {
        $select = "SELECT COUNT(*) as banquet_count FROM entity_master_list WHERE status = 'ACTIVE'";
        $banquet_count_stmt = $this->db->conn_id->prepare($select);
        $banquet_count_stmt->execute();
        $banquet_count = $banquet_count_stmt->fetch(PDO::FETCH_ASSOC);
        //echo var_export($banquet_count_stmt->errorInfo());
        return $banquet_count;
    }

    function get_user_count() {
        $select = "SELECT COUNT(*) as user_count FROM `user_master_list` WHERE status = 'ACTIVE'";
        $user_count_stmt = $this->db->conn_id->prepare($select);
        $user_count_stmt->execute();
        $user_count = $user_count_stmt->fetch(PDO::FETCH_ASSOC);
        //echo var_export($user_count_stmt->errorInfo());
        return $user_count;
    }

    function get_em_count() {
        $select = "SELECT COUNT(*) as em_count FROM `partner_user_list` WHERE status = 'ACTIVE' AND user_type = 4";
        $em_count_stmt = $this->db->conn_id->prepare($select);
        $em_count_stmt->execute();
        $em_count = $em_count_stmt->fetch(PDO::FETCH_ASSOC);
        //echo var_export($user_count_stmt->errorInfo());
        return $em_count;
    }
    
    function get_user_chat_count($id) {
        $select = "SELECT COUNT(*) as em_count FROM `chat` WHERE status = '0' AND receiver_id = ?";
        $em_count_stmt = $this->db->conn_id->prepare($select);
        $em_count_stmt->execute(array($id));
        $em_count = $em_count_stmt->fetch(PDO::FETCH_ASSOC);
        //echo var_export($user_count_stmt->errorInfo());
        return $em_count;
    }

    function get_booking_count() {
        $select = "SELECT COUNT(*) as booking_count FROM customer_requests"; //  WHERE status = '5'
        $booking_count_stmt = $this->db->conn_id->prepare($select);
        $booking_count_stmt->execute();
        $booking_count = $booking_count_stmt->fetch(PDO::FETCH_ASSOC);
        //echo var_export($user_count_stmt->errorInfo());
        return $booking_count;
    }

    function get_event_type_for_search_form() {
        $amenities = "SELECT id, `type` FROM event_type  WHERE STATUS = 'ACTIVE'  ORDER BY display_order ASC ";
        $stmt = $this->db->conn_id->prepare($amenities);
        $stmt->execute();
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_guests_for_search_form() {
        $amenities = "SELECT id, capacity FROM `guest_capacity`    ORDER BY `orderby` ASC  ";
        $stmt = $this->db->conn_id->prepare($amenities);
        $stmt->execute();
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function is_message_member($hash, $user_id) {
        $amenities = "SELECT * FROM `messages` WHERE message_hash = :message_hash
                          AND (sender_id = :sender_id AND receiver_id = :receiver_id) ";
        $stmt = $this->db->conn_id->prepare($amenities);
        $stmt->execute(
            array(
                ':message_hash' => $hash,
                ':sender_id' => $user_id,
                ':receiver_id' => $user_id
            )
        );
        return $stmt->fetchALL(PDO::FETCH_ASSOC);
    }

    function get_message_sender_receiver($hash, $sender) {
        $sender = get_session_user_id();
        $amenities = "SELECT * FROM `messages` WHERE message_hash = :message_hash
                          AND (sender_id = :sender_id OR receiver_id = :sender_id) LIMIT 1";
        $stmt = $this->db->conn_id->prepare($amenities);
        $stmt->execute(
            array(
                ':message_hash' => $hash,
                ':sender_id' => $sender
            )
        );
        $get_message_receiver = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($get_message_receiver !== false) {
            $receiver = ($get_message_receiver['sender_id'] == $sender) ?
                $get_message_receiver['receiver_id']
                : $get_message_receiver['sender_id'];
            return array('sender_id' => $sender, 'receiver_id' => $receiver);
        } else {
            return false;
        }
    }

    function get_event_type_info($event_type_id) {
        $amenities = "SELECT * FROM `event_type` WHERE id = :event_type_id ";
        $stmt = $this->db->conn_id->prepare($amenities);
        $stmt->execute(
            array(
                ':event_type_id' => $event_type_id
            )
        );
        $get_message_receiver = $stmt->fetch(PDO::FETCH_ASSOC);
        return $get_message_receiver['type'];
    }

    function get_additional_services_info($additional_services_list) {
        $amenities = "SELECT * FROM `service_master` WHERE id IN ('" . $additional_services_list . "') ";
        $stmt = $this->db->conn_id->prepare($amenities);
        $stmt->execute();
        $get_message_receiver = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $get_message_receiver;
    }

    function get_recent_venue_providers() {
        $query = "SELECT * FROM `user_master_list` WHERE user_type = 'PARTNER' ORDER BY id DESC limit 5";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function get_recent_vendors() {
        $query = "SELECT * FROM `user_master_list` WHERE user_type = 'VENDORS' ORDER BY id DESC limit 5";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function get_recent_users() {
        $query = "SELECT * FROM `user_master_list` WHERE user_type = 'USER' ORDER BY id DESC limit 5";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function get_my_recent_customer_requests($user_id) {
        $query = "SELECT * FROM `customer_requests_list` WHERE user_id = ? ORDER BY id DESC limit 5";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($user_id));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function get_my_recent_confirmed_bookings($user_id) {
        $query = "SELECT * FROM `customer_requests_list` WHERE user_id = ? AND request_process = 'BOOKING_CONFIRM_CUSTOMER' ORDER BY id DESC limit 5";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($user_id));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function get_my_recent_payments($user_id) {
        $query = "SELECT * FROM `payment_list` WHERE user_id = ?  ORDER BY id DESC limit 5";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute(array($user_id));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function get_promo_code() {
        $query = "SELECT * FROM `promo_list` WHERE CURDATE() BETWEEN `start_date` AND `end_date`";
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data;
    }
}
